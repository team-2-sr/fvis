import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.isUserLoggedIn() || this.auth.loggedInUser$.value.role !== 2) {
      this.router.navigate(['home']);
      return false;
    }

    return true;
  }
}
