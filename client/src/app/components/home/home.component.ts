import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private loggedInSubscription: Subscription;

  public loggedIn: boolean;
  public assigned: number;
  public unassigned: number;

  constructor(private readonly auth: AuthService,
              private readonly productsService: ProductsService) { }
  // authService = this.auth;
  ngOnInit() {
    this.loggedInSubscription = this.auth.isLoggedIn$
        .subscribe(
          loggedIn => this.loggedIn = loggedIn
        );

    this.productsService.getAllproducts()
      .subscribe(products => {
        this.assigned = products.filter(product => product.assignedCves.length > 0).length;
        this.unassigned = products.filter(product => product.assignedCves.length === 0).length;
      });
  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
  }

}
