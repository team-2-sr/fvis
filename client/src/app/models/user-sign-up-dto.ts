export class UserSignUpDto{

    name: string;
    username: string;
    password: string;
    email: string;
}
