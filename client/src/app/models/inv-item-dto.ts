import { CpeDto } from './cpe-dto';
import { CveDto } from './cve-dto';

export class InventoryItemDto {
    id: number;
    product: string;
    vendor: string;
    version: string;
    cpe: CpeDto | null;
    assignedCves: CveDto[];
}