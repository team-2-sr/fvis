
export class CpeDto {
    public id: number;
    public cpe23Uri: string;
    public title: string;
    public vendor: string;
    public product: string;
    public version: string;
    public update: string;
    public deprecated: boolean;
    public depricatedBy?: string | null;
    public depricatedOn?: string | null;
}
