export class SearchDto {
    cpe23Uri?: string;
    title?: string;
    cveId?: string;
    product?: string;
    vendor?: string;
}
