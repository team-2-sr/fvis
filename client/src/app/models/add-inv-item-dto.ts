export class AddInventoryItemDto {
    product: string;
    vendor: string;
    version: string;
}