import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router';
import { AllProductsComponent } from './all-products/all-products.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AuthGuardService } from '../../guards/auth-guard-service';
import { ProductsResolverService } from './products-resolver.service';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductDetailsResolverService } from './prod-details-resolver.service';

const productsRoutes: Routes = [
  { path: '', component: AllProductsComponent, resolve: { products: ProductsResolverService },
                                              pathMatch: 'full', canActivate: [AuthGuardService] },
  { path: 'assigned', component: AllProductsComponent, resolve: { products: ProductsResolverService }, canActivate: [AuthGuardService] },
  { path: 'unassigned', component: AllProductsComponent, resolve: { products: ProductsResolverService }, canActivate: [AuthGuardService] },
  { path: 'add-product', component: AddProductComponent, canActivate: [AuthGuardService] },
  { path: ':id', component: ProductDetailsComponent,  resolve: { details: ProductDetailsResolverService },
                                                      canActivate: [AuthGuardService] }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(productsRoutes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ProductsRouterModule { }
