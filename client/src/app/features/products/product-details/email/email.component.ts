import { Component, OnInit, Input } from '@angular/core';
import { NotificatorService } from '../../../core/services/notificator.service';
import { AssignmentsService } from '../../../../services/assignments.service';
import { InventoryItemDto } from '../../../../models/inv-item-dto';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css'],
})
export class EmailComponent implements OnInit {
  public hasError: boolean;
  public message = ``;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly assignmentsService: AssignmentsService
  ) {}

  @Input() product: InventoryItemDto;

  ngOnInit(): void {}

  public sendEmail(productId: number) {
    this.assignmentsService.sendEmailForProduct(productId).subscribe(
      () => this.notificator.success(`Email has been sent!`),

      (e) => {
        if (e.error.statusCode === 404) {
          this.hasError = true;
          this.message = `Something's wrong!`;
        }
      }
    );
  }
}
