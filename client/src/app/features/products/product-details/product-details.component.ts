import { Subscription } from 'rxjs';
import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import { ProductsService } from '../../../services/products.service';
import { ActivatedRoute } from '@angular/router';
import { NotificatorService } from '../../core/services/notificator.service';
import { InventoryItemDto } from '../../../models/inv-item-dto';
import { CpeDto } from '../../../models/cpe-dto';
import { CveDto } from '../../../models/cve-dto';
import { AssignmentsService } from '../../../services/assignments.service';
import { RemoveCpeDialogComponent } from '../remove-cpe-dialog/remove-cpe-dialog.component';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  public product: InventoryItemDto;
  public assignedCpe: CpeDto | null;
  public assignedCves: CveDto[] = [];
  public cpeMatches: CpeDto[] = [];
  public cveMatches: CveDto[] = [];

  constructor(
    private readonly assignmentsService: AssignmentsService,
    private readonly route: ActivatedRoute,
    private readonly notificator: NotificatorService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(
      ({ details }) => {
        details.subscribe(([product, cpes, cves]) => {
          this.product = product;
          this.assignedCpe = product.cpe;
          this.assignedCves = product.assignedCves;

          if (this.assignedCpe) {
            cpes = cpes.filter(cpe => cpe.id !== this.assignedCpe.id);
            cpes.unshift(this.assignedCpe);
          }

          this.cpeMatches = cpes;

          if (cves) {
            if (this.assignedCves.length) {
              const assignedCveIds = this.assignedCves.map(cve => cve.id);
              cves = cves.filter(cve => !assignedCveIds.includes(cve.id));
            }

            this.cveMatches = [...this.assignedCves, ...cves];
          }
        },
        (error) => {
          console.log(error.error?.error);
          this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
        }
        );
      }
    );
  }

  assignCpe(cpeId: number) {
    if (!this.assignedCves.length) {
      this.assignmentsService.assignProductCpe(this.product.id, cpeId)
        .subscribe(cpe => {
          this.assignedCpe = cpe;
          this.assignmentsService.getCveMatches(this.assignedCpe.id)
            .subscribe(cves => {
              this.cveMatches = cves;
            },
            (error) => {
              console.log(error.error?.error);
              this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
            }
          );
        },
        (error) => {
          console.log(error.error?.error);
          this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
        }
        );
    } else {
      this.dialog.open(RemoveCpeDialogComponent);
    }
  }

  unassignCpe() {
    if (this.assignedCves.length === 0) {
      this.assignmentsService.removeProductCpe(this.product.id)
        .subscribe(msg => {
          this.assignedCpe = null;
          this.cveMatches = [];
        },
        (error) => {
          console.log(error.error?.error);
          this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
        }
        );
    } else {
        this.dialog.open(RemoveCpeDialogComponent);
    }

  }

  assignCve(cveId: number) {
    this.assignmentsService.assignProductCve(this.product.id, cveId)
      .subscribe(cves => {
        this.assignedCves = [...this.assignedCves, ...cves];
      },
      (error) => {
        console.log(error.error?.error);
        this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
      }
      );
  }

  unassignCve(cveId: number) {
    this.assignmentsService.removeProductCve(this.product.id, cveId)
      .subscribe(msg => {
        this.assignedCves = this.assignedCves.filter(cve => cve.id !== cveId);
      },
      (error) => {
        console.log(error.error?.error);
        this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
      }
      );
  }

  checkCveAssignment(assignedCves: CveDto[], cve: CveDto): boolean {
    const assignedCveIds = assignedCves.map(assignedCve => assignedCve.id);
    return assignedCveIds.includes(cve.id);
  }
}





// ngOnInit(): void {
//   this.route.data.subscribe(
//     ({details}) => {
//       let product: InventoryItemDto;
//       let cpes: CpeDto[];
//       let cves: CveDto[];

//       [product, cpes, cves] = details;
//       this.product = product;
//       this.assignedCpe = product.cpe;
//       this.assignedCves = product.assignedCves;

//       if (this.assignedCpe) {
//         cpes = cpes.filter(cpe => cpe.id !== this.assignedCpe.id);
//         cpes.unshift(this.assignedCpe);
//       }

//       this.cpeMatches = cpes;

//       if (cves) {
//         if (this.assignedCves.length) {
//           const assignedCveIds = this.assignedCves.map(cve => cve.id);
//           cves = cves.filter(cve => !assignedCveIds.includes(cve.id));
//         }

//         this.cveMatches = [...this.assignedCves, ...cves];
//       }
//     },
//     (error) => {
//       console.log(error.error?.error);
//       this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
//     }
//   );
// }
