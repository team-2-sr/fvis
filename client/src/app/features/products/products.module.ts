import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material.module';
import { ProductsRouterModule } from './products-router.module';
import { AddProductComponent } from './add-product/add-product.component';
import { AuthService } from '../../services/auth.service';
import { AllProductsComponent } from './all-products/all-products.component';
import { ProductsResolverService } from './products-resolver.service';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductDetailsResolverService } from './prod-details-resolver.service';
import { RemoveCpeDialogComponent } from './remove-cpe-dialog/remove-cpe-dialog.component';
import { EmailComponent } from './product-details/email/email.component';



@NgModule({
  declarations: [
    AllProductsComponent,
    AddProductComponent,
    ProductDetailsComponent,
    RemoveCpeDialogComponent,
    EmailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ProductsRouterModule
  ],
  providers: [
    AuthService,
    ProductsResolverService,
    ProductDetailsResolverService
  ],

  exports: [],
})
export class ProductsModule { }
