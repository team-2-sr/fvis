import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router, ActivatedRoute } from '@angular/router';
import { Observable, of, EMPTY, combineLatest  } from 'rxjs';
import { Injectable } from '@angular/core';
import { InventoryItemDto } from '../../models/inv-item-dto';
import { ProductsService } from '../../services/products.service';
import { NotificatorService } from '../core/services/notificator.service';
import { catchError, map } from 'rxjs/operators';
import { AssignmentsService } from '../../services/assignments.service';

@Injectable()
export class ProductDetailsResolverService implements Resolve<InventoryItemDto[]> {

  constructor(
    private readonly productsService: ProductsService,
    private readonly assignmentsService: AssignmentsService,
    private readonly router: Router,
    private readonly notificator: NotificatorService) { }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;

    return this.productsService.getOneProduct(id)
        .pipe(
            map(product => {
                if (product.cpe) {
                    return combineLatest([of(product),
                        this.assignmentsService.getCpeMatches(product.product, product.vendor, product.version),
                        this.assignmentsService.getCveMatches(product.cpe.id)]);
                }

                return combineLatest([of(product),
                    this.assignmentsService.getCpeMatches(product.product, product.vendor, product.version)]);
            }),
            catchError(error => {
                console.error(`Can't resolve product details because of the error: ${error.error?.error}`);
                this.router.navigate(['/home']);
                this.notificator.error(`There was unexpected error.`);
                return of(null);
              })
        );
  }
}






// return new Observable<InventoryItemDto[]>((observer ) => {
//   this.productsService.getOneProduct(id)
//     .pipe(
//         map(product => {
//             if (product.cpe) {
//                 return combineLatest([of(product),
//                     this.assignmentsService.getCpeMatches(product.product, product.vendor, product.version),
//                     this.assignmentsService.getCveMatches(product.cpe.id)]);
//             }

//             return combineLatest([of(product),
//                 this.assignmentsService.getCpeMatches(product.product, product.vendor, product.version)]);
//         }),
//         catchError(error => {
//             console.error(`Can't resolve product details because of the error: ${error.error?.error}`);
//             this.router.navigate(['/home']);
//             this.notificator.error(`There was unexpected error.`);
//             return of(null);
//           })
//     )
//       .subscribe(data => {
//         data.subscribe(details => {
//           observer.next(details);
//           observer.complete();
//         });
//       });
// });
// }