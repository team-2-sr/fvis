import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router, ActivatedRoute } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { Injectable } from '@angular/core';
import { InventoryItemDto } from '../../models/inv-item-dto';
import { ProductsService } from '../../services/products.service';
import { NotificatorService } from '../core/services/notificator.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ProductsResolverService implements Resolve<InventoryItemDto[]> {

  constructor(
    private readonly productsService: ProductsService,
    private readonly router: Router,
    private readonly notificator: NotificatorService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return this.productsService.getAllproducts()
        .pipe(
          catchError(error => {
            console.error(`Can't resolve All products because of the error: ${error.error?.error}`);
            this.router.navigate(['/home']);
            this.notificator.error(`There was unexpected error.`);
            return of(null);
          })
        );
  }
}
