import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InventoryItemDto } from '../../../models/inv-item-dto';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css']
})
export class AllProductsComponent implements OnInit {

  public itemMatch: string;
  public products: InventoryItemDto[];

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.itemMatch = this.router.url.split('/')[2];

    this.route.data.subscribe(
        ({ products }) => {
          if (this.itemMatch === 'assigned') {
            this.products = products.filter(product => product.assignedCves.length > 0);
          } else if (this.itemMatch === 'unassigned') {
            this.products = products.filter(product => product.assignedCves.length === 0);
          } else {
            this.products = products;
          }
        }
      );
  }

}
