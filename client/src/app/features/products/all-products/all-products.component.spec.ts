import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProductsComponent } from './all-products.component';
import { of } from 'rxjs';
import { InventoryItemDto } from '../../../models/inv-item-dto';
import { CveDto } from '../../../models/cve-dto';
import { CpeDto } from '../../../models/cpe-dto';
import { MaterialModule } from '../../shared/material.module';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

const cpe: CpeDto = {
  id: 1,
  cpe23Uri: 'test',
  title: 'test',
  vendor: 'Git-scm',
  product: 'Git',
  version: '-',
  update: 'test',
  deprecated: false,
  depricatedBy: null,
  depricatedOn: null
}

const cve: CveDto = {
  id: 1,
  cveID: 'test',
  description: 'test',
  severity: 'Low',
  lastModifiedDate: 'test',
}


const products: InventoryItemDto[] = [{
  id: 1,
  product: 'Git',
  vendor: 'Git-scm',
  version: '1.1.1', 
  cpe,
  assignedCves: [cve]
},
{
  id: 2,
  product: 'Test',
  vendor: 'Test',
  version: '1',
  cpe: null,
  assignedCves: []
}
];

describe('AllProductsComponent', () => {
  let component: AllProductsComponent;
  let fixture: ComponentFixture<AllProductsComponent>;

  const route = ({ data: of({ products }) } as any) as ActivatedRoute;

  const router = {
    url : '/products/'
    };

  beforeEach(async(() => {
    // jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [MaterialModule, RouterTestingModule],
      declarations: [ AllProductsComponent ],
      providers: [ ]
    })
    .overrideProvider(Router, { useValue: router })
    .overrideProvider(ActivatedRoute, { useValue: route })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should update itemMatch on init', (done) => {
       // Act & Assert
       route.data.subscribe(state => {
        expect(component.itemMatch).toBe('');
        done();
      });
    });

    it('should update products on init', (done) => {
      // Act & Assert
      route.data.subscribe(state => {
       route.data.subscribe( stat => {
         expect(component.products).toBe(products);
         done();
       });
       done();
     });
   });
  });

});
