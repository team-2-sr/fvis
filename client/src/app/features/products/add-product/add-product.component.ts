import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from '../../../services/products.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  productForm: FormGroup;

  constructor(private readonly fb: FormBuilder,
              private readonly productsService: ProductsService,
              private readonly notificator: NotificatorService) { }

  ngOnInit() {
    this.productForm = this.fb.group({
      product: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      vendor: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      version: ['', Validators.compose([Validators.required])],
    });
  }

  addProduct() {
    const invItem = {
      product: this.productForm.value.product,
      vendor: this.productForm.value.vendor,
      version: this.productForm.value.version
    };

    this.productsService.addProduct(invItem)
      .subscribe(product => {
        this.notificator.success('Product succesfully added!');
        this.productForm.reset();
      },
     (error) => (console.log(error.error?.error), this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`))
    );
  }
}
