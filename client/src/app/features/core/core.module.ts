import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconGenerateService } from './services/icon-generate.service';
import { NotificatorService } from './services/notificator.service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [],
  providers: [ IconGenerateService, NotificatorService],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
      enableHtml: true
    }),
  ]
})
export class CoreModule {
  constructor(private iconGenService: IconGenerateService) {
    this.iconGenService.init();
  }
}
