import { Injectable } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';

@Injectable({
  providedIn: 'root'
})
export class IconGenerateService {

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) {
  }

  init() {
    this.iconRegistry.addSvgIcon(
        'alarm_on',
        this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/alarm_on-24px.svg'));

    this.iconRegistry.addSvgIcon(
        'done_outline',
        this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/done_outline-24px.svg'));

    this.iconRegistry.addSvgIcon(
        'alarm_add',
        this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/alarm_add-24px.svg'));

    this.iconRegistry.addSvgIcon(
        'alarm_off',
        this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/alarm_off-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'how_to_reg',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/how_to_reg-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'reply',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/reply-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'edit',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/edit-24px.svg'));


    this.iconRegistry.addSvgIcon(
      'view-list',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/view_list-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'list',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/list-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'post-add',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/post_add-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'home',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/home-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'perm-identity',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/perm_identity-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'delete_forever',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/delete_forever-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'delete',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/delete-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'add',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/add-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'search',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/search-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'favorite',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/favorite-24px.svg'));

    this.iconRegistry.addSvgIcon(
      'favorite-border',
      this.sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/favorite_border-24px.svg'));
  }
}
