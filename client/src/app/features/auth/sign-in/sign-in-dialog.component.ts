import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'sign-in-dialog',
  templateUrl: './sign-in-dialog.html',
})
export class SignInDialog {

  public userCredentialsForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(16)]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    public dialogRef: MatDialogRef<SignInDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface DialogData {
  username: string;
  password: string;
  name?: string;
  email?: string;
}
