import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../sign-in/sign-in-dialog.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'sign-up-dialog',
  templateUrl: './sign-up-dialog.html',
})
export class SignUpDialog {

  public userCredentialsFormSignUp: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(16)]),
    password: new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3,32}$/)]),
    name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(35)]),
    email: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(60), Validators.pattern(/\S+@\S+\.\S+/)]),
  });

  constructor(
    public dialogRef: MatDialogRef<SignUpDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

