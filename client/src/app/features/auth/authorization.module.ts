import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationComponent } from './authorization.component';
import { SignInDialog } from './sign-in/sign-in-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../shared/material.module';
import { SignUpDialog } from './sign-up/sign-up-dialog.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';


@NgModule({
  declarations: [AuthorizationComponent, SignInDialog, SignUpDialog, NavBarComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    MaterialModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        }
    }
   })
  ],
  providers: [],
  exports: [AuthorizationComponent, SignInDialog, FormsModule, SignUpDialog, NavBarComponent]
})
export class AuthorizationModule { }
