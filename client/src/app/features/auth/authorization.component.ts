import { Component, OnInit, OnDestroy } from '@angular/core';
import { SignInDialog } from './sign-in/sign-in-dialog.component';
import { AuthService } from '../../services/auth.service';
import { StorageService } from '../../services/storage.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SignUpDialog } from './sign-up/sign-up-dialog.component';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css'],
})
export class AuthorizationComponent implements OnInit, OnDestroy {
  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public user;
  public loggedIn: boolean;

  constructor(
    public dialog: MatDialog,
    private readonly auth: AuthService,
    private readonly storage: StorageService,
    private readonly router: Router
  ) {}

  authService = this.auth;

  public ngOnInit() {
    this.loggedInSubscription = this.auth.isLoggedIn$.subscribe(
      (loggedIn) => (this.loggedIn = loggedIn)
    );
    this.userSubscription = this.auth.loggedInUser$.subscribe(
      (user) => (this.user = user)
    );
  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SignInDialog, {
      width: '250px',
      data: { user: this.user },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== undefined) {
        this.auth
          .loginUser({ username: result?.username, password: result?.password })
          .subscribe((response) => {
            this.storage.setItem('token', response.token);
            const user = this.auth.getUserFromToken(response.token);
            this.auth.loggedInUser$.next(user);
            this.router.navigate(['']);
            this.loggedIn = true;
          });
      }
    });
  }

  openDialogforSignUp(): void {
    const dialogRef = this.dialog.open(SignUpDialog, {
      width: '250px',
      data: { user: this.user },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== undefined) {
        this.auth
          .registerUser({
            username: result?.username,
            password: result?.password,
            name: result?.name,
            email: result?.email,
          })
          .subscribe();
      }
    });
  }

  public logout() {
    this.auth.logout();
    this.loggedIn = false;
  }
}
