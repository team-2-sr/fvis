import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private readonly activatedRoute: ActivatedRoute) { }

  @Input() loggedIn: boolean;

  public isHomePage;

  ngOnInit(): void {
    this.isHomePage = this.activatedRoute.snapshot['_routerState'].url;
  }

  public isHome() : boolean{
    return this.activatedRoute.snapshot['_routerState'].url === "/home"
  }

  public isAllProducts() : boolean{
    return this.activatedRoute.snapshot['_routerState'].url === "/products"
  }

  public isAddProduct() : boolean{
    return this.activatedRoute.snapshot['_routerState'].url === "/products/add-product"
  }

  public isSearchPage() : boolean{
    return this.activatedRoute.snapshot['_routerState'].url === "/search"
  }
}
