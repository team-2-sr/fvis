import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  private readonly baseUrl = 'http://localhost:3000/api/update';

  constructor(private readonly http: HttpClient) {}

  scheduleCpesUpdate(time: number): Observable<{message: string}> {
    return this.http.put<{message: string}>(`${this.baseUrl}/cpe/updatetime?time=${time}`, {});
  }

  scheduleCvesUpdate(time: number): Observable<{message: string}> {
    return this.http.put<{message: string}>(`${this.baseUrl}/cve/updatetime?time=${time}`, {});
  }
}

