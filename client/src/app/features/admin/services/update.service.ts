import { delay } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {
  private readonly baseUrl = 'http://localhost:3000/api/update';

  constructor(private readonly http: HttpClient) { }

  seedCPES(): Observable<{message: string}> {
    return this.http.post<{message: string}>(`${this.baseUrl}/cpe`, {});
  }

  seedCVES(): Observable<{message: string}> {
    return this.http.post<{message: string}>(`${this.baseUrl}/cve`, {});
  }

  checkCpeTimestamp(options: {seeding: boolean}): Observable<string> {
    return this.http.get<string>(`${this.baseUrl}/cpe/timestamp?seeding=${options.seeding}`);
  }

  checkCveTimestamp(options: {seeding: boolean}): Observable<string> {
    return this.http.get<string>(`${this.baseUrl}/cve/timestamp?seeding=${options.seeding}`);
  }
}
