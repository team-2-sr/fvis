import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminGuardService } from '../../guards/admin-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';

const adminRoutes: Routes = [
  { path: '', component: AdminComponent, pathMatch: 'full', canActivate: [AdminGuardService] },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(adminRoutes),
  ],
  exports: [
    RouterModule,
  ],
})
export class AdminRouterModule { }
