import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { UpdateComponent } from './update/update.component';
import { AdminRouterModule } from './admin-router.module';
import { MaterialModule } from '../shared/material.module';
import { SeedStartDialogComponent } from './update/seed-start-dialog/seed-start-dialog.component';
import { SchedulerComponent } from './scheduler/scheduler.component';

@NgModule({
  declarations: [AdminComponent, UpdateComponent, SeedStartDialogComponent, SchedulerComponent],
  imports: [
    CommonModule,
    MaterialModule,
    AdminRouterModule
  ],
})
export class AdminModule { }
