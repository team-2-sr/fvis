import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SeedStartDialogComponent } from './seed-start-dialog/seed-start-dialog.component';
import { UpdateService } from '../services/update.service';
import { subscribeOn, timestamp } from 'rxjs/operators';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  public seedCpeInProgress: boolean;
  public seedCveInProgress: boolean;
  public cpeMsg: string;
  public cveMsg: string;

  constructor(public dialog: MatDialog,
              private readonly updateService: UpdateService,
              private readonly notificator: NotificatorService) { }

  ngOnInit(): void {
    this.updateService.checkCpeTimestamp({seeding: true})
      .subscribe(timestampCPE => {
        this.seedCpeInProgress = timestampCPE ? true : false;
      });

    this.updateService.checkCveTimestamp({seeding: true})
    .subscribe(timestampCVE => {
      this.seedCveInProgress = timestampCVE ? true : false;
    });
  }

  seedCPES() {
    if (!this.seedCpeInProgress) {
      const dialogRef = this.dialog.open(SeedStartDialogComponent);
      this.cpeMsg = '';

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.seedCpeInProgress = true;
          this.updateService.seedCPES()
            .subscribe(res => {
              this.cpeMsg = res.message;
            },
          (error) => {
            console.log(error.error?.error);
            setTimeout(() => {
              this.seedCpeInProgress = false;
              this.cpeMsg = `${error.error?.error ? error.error?.error : ''}`;
            }, 1000);
            if (!error.error?.error) {
              this.notificator.error(`Something went wrong`);
            }
          });
        }
      });
    }
  }

  seedCVES() {
    if (!this.seedCveInProgress) {
      const dialogRef = this.dialog.open(SeedStartDialogComponent);
      this.cveMsg = '';

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.seedCveInProgress = true;
          this.updateService.seedCVES()
            .subscribe(res => {
              this.cveMsg = res.message;
            },
          (error) => {
            console.log(error.error?.error);
            setTimeout(() => {
              this.seedCveInProgress = false;
              this.cveMsg = `${error.error?.error ? error.error?.error : ''}`;
            }, 1000);
            if (!error.error?.error) {
              this.notificator.error(`Something went wrong`);
            }
          });
        }
      });
    }
  }

  checkCpeTimestamp() {
    this.updateService.checkCpeTimestamp({seeding: true})
      .subscribe(res => {
        if (res) {
          this.cpeMsg = 'CPEs seeding...';
        } else {
          this.seedCpeInProgress = false;
          this.cpeMsg = 'CPEs seeded';
        }
      });
  }

  checkCveTimestamp() {
    this.updateService.checkCveTimestamp({seeding: true})
      .subscribe(res => {
        if (res) {
          this.cveMsg = 'CVEs seeding...';
        } else {
          this.seedCveInProgress = false;
          this.cveMsg = 'CVEs seeded';
        }
      });
  }

}
