export enum Intervals {
    HOURLY = 3600000,
    DAILY =  86400000,
    WEEKLY = 604800000
}