import { Component, OnInit } from '@angular/core';
import { Intervals } from '../common/enum/intervals';
import { ScheduleService } from '../services/schedule.service';
import { subscribeOn } from 'rxjs/operators';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerComponent implements OnInit {

  cpeInterval: string;
  cveInterval: string;
  cpeMsg: string;
  cveMsg: string;

  constructor(private readonly scheduleService: ScheduleService,
              private readonly notificator: NotificatorService) { }

  ngOnInit(): void {
  }

  scheduleCpeUpdate() {
    if (Object.keys(Intervals).includes(this.cpeInterval)) {
      this.scheduleService.scheduleCpesUpdate(Intervals[this.cpeInterval])
        .subscribe(msg => {
          this.notificator.success(msg.message);
          this.cpeMsg = msg.message;
        },
        (error) => {
          console.log(error.error?.error);
          this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
        }
        );
    }
  }

  scheduleCveUpdate() {
    if (Object.keys(Intervals).includes(this.cveInterval)) {
      this.scheduleService.scheduleCvesUpdate(Intervals[this.cveInterval])
        .subscribe(msg => {
          this.notificator.success(msg.message);
          this.cveMsg = msg.message;
        },
        (error) => {
          console.log(error.error?.error);
          this.notificator.error(error.error?.error ? error.error?.error : `Something went wrong`);
        }
        );
    }
  }

  checkInterval(key: string) {
    return !Object.keys(Intervals).includes(key);
  }
}
