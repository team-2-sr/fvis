import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    NgxSpinnerModule,
  ],
  exports: [
    FormsModule,
    MaterialModule,
    HttpClientModule,
    NgxSpinnerModule,
  ]
})
export class SharedModule {

  constructor(@Optional() @SkipSelf() parent: SharedModule) {
    if (parent) {
      throw new Error(`SharedModule has already been initialized!`);
    }
  }
 }
