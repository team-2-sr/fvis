import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { InventoryItemDto } from '../../models/inv-item-dto';
import { SearchDto } from '../../models/search-dto';
import { NotificatorService } from '../core/services/notificator.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  public products: InventoryItemDto[];
  public search: SearchDto = {
    cpe23Uri: '',
    title: '',
    cveId: '',
    product: '',
    vendor: '',
  };
  public hasError: boolean;
  public message = ``;
  public buttonValue: number = 0;

  constructor(
    private readonly searchService: SearchService,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit() {}

  public radioButtonByVendor() {
    this.buttonValue = 1;
  }

  public radioButtonByProduct() {
    this.buttonValue = 2;
  }

  public radioButtonByCpeUri() {
    this.buttonValue = 3;
  }

  public radioButtonByTitle() {
    this.buttonValue = 4;
  }

  public radioButtonByCveId() {
    this.buttonValue = 5;
  }

  public searchProductByCpe(search: SearchDto) {
    if (search.cpe23Uri && this.buttonValue === 3) {
      this.searchService.searchByCpe(search).subscribe({
        next: (data) => {
          this.products = data;
          this.search = {
            cpe23Uri: '',
            title: '',
            cveId: '',
            product: '',
            vendor: '',
          };
          if (this.products.length === 0) {
            try {
              throw new Error();
            } catch (error) {
              this.notificator.error(
                error.error?.error
                  ? error.error?.error
                  : `No results were found`
              );
            }
          }
        },

        error: (e) => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = `Something's wrong!`;
          }
        },
      });
    }

    if (search.title && this.buttonValue === 4) {
      this.searchService.searchByCpe(search).subscribe({
        next: (data) => {
          this.products = data;
          this.search = {
            cpe23Uri: '',
            title: '',
            cveId: '',
            product: '',
            vendor: '',
          };
          if (this.products.length === 0) {
            try {
              throw new Error();
            } catch (error) {
              this.notificator.error(
                error.error?.error
                  ? error.error?.error
                  : `No results were found`
              );
            }
          }
        },

        error: (error) => (
          console.log(error.error?.error),
          this.notificator.error(
            error.error?.error ? error.error?.error : `Something's wrong!`
          )
        ),
      });
    }
  }

  public searchProductByCveId(search: SearchDto) {
    if (search.cveId && this.buttonValue === 5) {
      this.searchService.searchByCveId(search).subscribe({
        next: (data) => {
          this.products = data.filter(
            (e, i, a) => a.findIndex((t) => t.id === e.id) === i
          );
          this.search = {
            cpe23Uri: '',
            title: '',
            cveId: '',
            product: '',
            vendor: '',
          };
          if (this.products.length === 0) {
            try {
              throw new Error();
            } catch (error) {
              this.notificator.error(
                error.error?.error
                  ? error.error?.error
                  : `No results were found`
              );
            }
          }
        },

        error: (e) => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = `Something's wrong!`;
          }
        },
      });
    }
  }
  public searchProductByProduct(search: SearchDto) {
    if (search.product && this.buttonValue === 2) {
      this.searchService.searchByProduct(search).subscribe(
        data => {
          this.products = data;
          this.search = {
            cpe23Uri: '',
            title: '',
            cveId: '',
            product: '',
            vendor: '',
          };
          if (this.products.length === 0) {
            try {
              throw new Error();
            } catch (error) {
              this.notificator.error(
                error.error?.error
                  ? error.error?.error
                  : `No results were found`
              );
            }
          }
        },

        (e) => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = `Something's wrong!`;
          }
        },
      );
    }
  }

  public searchProductByVendor(search: SearchDto) {
    if (search.vendor && this.buttonValue === 1) {
      this.searchService.searchByVendor(search).subscribe({
        next: (data) => {
          this.products = data;
          this.search = {
            cpe23Uri: '',
            title: '',
            cveId: '',
            product: '',
            vendor: '',
          };
          if (this.products.length === 0) {
            try {
              throw new Error();
            } catch (error) {
              this.notificator.error(
                error.error?.error
                  ? error.error?.error
                  : `No results were found`
              );
            }
          }
        },

        error: (e) => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = `Something's wrong!`;
          }
        },
      });
    }
  }
}
