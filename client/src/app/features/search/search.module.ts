import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material.module';
import { SearchComponent } from './search.component';
import { AuthService } from '../../services/auth.service';
import { SearchRouterModule } from './search-router.module';


@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SearchRouterModule
  ],
  providers: [
    AuthService,
  ],

  exports: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class SearchModule { }
