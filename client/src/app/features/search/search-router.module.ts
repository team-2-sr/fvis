import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router';
import { SearchComponent } from './search.component';
import { AuthGuardService } from '../../guards/auth-guard-service';

const searchRoutes: Routes = [
  { path: '', component: SearchComponent, pathMatch: 'full',canActivate: [AuthGuardService] },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(searchRoutes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SearchRouterModule { }
