import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InventoryItemDto } from '../models/inv-item-dto';
import { Observable } from 'rxjs';
import { SearchDto } from '../models/search-dto';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private readonly baseUrl = 'http://localhost:3000/api/search';

  constructor(private readonly http: HttpClient) {}

  searchByCpe(search: SearchDto): Observable<InventoryItemDto[]> {
    const query = `title=${search.title}&cpe23Uri=${search.cpe23Uri}`;

    return this.http.get<InventoryItemDto[]>(`${this.baseUrl}/cpe?${query}`);
  }

  searchByCveId(search: SearchDto): Observable<InventoryItemDto[]> {
    const query = `cveId=${search.cveId}`;

    return this.http.get<InventoryItemDto[]>(`${this.baseUrl}/cve?${query}`);
  }

  searchByProduct(search: SearchDto): Observable<InventoryItemDto[]> {
    const query = `product=${search.product}`;

    return this.http.get<InventoryItemDto[]>(
      `${this.baseUrl}/products?${query}`
    );
  }

  searchByVendor(search: SearchDto): Observable<InventoryItemDto[]> {
    const query = `name=${search.vendor}`;

    return this.http.get<InventoryItemDto[]>(
      `${this.baseUrl}/products/vendor?${query}`
    );
  }
}
