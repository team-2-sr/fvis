import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InventoryItemDto } from '../models/inv-item-dto';
import { Observable } from 'rxjs';
import { AddInventoryItemDto } from '../models/add-inv-item-dto';
import { CpeDto } from '../models/cpe-dto';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private readonly baseUrl = 'http://localhost:3000/api/product';

  constructor(private readonly http: HttpClient) { }

  getAllproducts(): Observable<InventoryItemDto[]> {
    return this.http.get<InventoryItemDto[]>(this.baseUrl);
  }

  getOneProduct(id: string): Observable<InventoryItemDto> {
    return this.http.get<InventoryItemDto>(`${this.baseUrl}/${id}`);
  }

  addProduct(item: AddInventoryItemDto): Observable<InventoryItemDto> {
    return this.http.post<InventoryItemDto>(this.baseUrl, item);
  }
}
