import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InventoryItemDto } from '../models/inv-item-dto';
import { Observable } from 'rxjs';
import { AddInventoryItemDto } from '../models/add-inv-item-dto';
import { CpeDto } from '../models/cpe-dto';
import { CveDto } from '../models/cve-dto';

@Injectable({
  providedIn: 'root'
})
export class AssignmentsService {
  private readonly baseUrl = 'http://localhost:3000/api/product';

  constructor(private readonly http: HttpClient) { }

  getCpeMatches(product: string, vendor: string, version: string): Observable<CpeDto[]> {
    return this.http.get<CpeDto[]>(`${this.baseUrl}/cpes?product=${product}&vendor=${vendor}&version=${version}`);
  }

  assignProductCpe(productId: number, cpeId: number): Observable<CpeDto> {
    return this.http.put<CpeDto>(`${this.baseUrl}/${productId}/cpe/${cpeId}`, {});
  }

  removeProductCpe(productId: number): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`${this.baseUrl}/${productId}/cpe`, {});
  }

  getCveMatches(cpeId: number): Observable<CveDto[]> {
    return this.http.get<CveDto[]>(`${this.baseUrl}/cpe/${cpeId}/cve`);
  }

  assignProductCve(productId: number, cveId: number): Observable<CveDto[]> {
    return this.http.put<CveDto[]>(`${this.baseUrl}/${productId}/cve/${cveId}`, {});
  }

  removeProductCve(productId: number, cveId: number): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`${this.baseUrl}/${productId}/cve/${cveId}`, {});
  }

  sendEmailForProduct(productId: number): Observable<{message: string}> {
    return this.http.put<{message: string}>(`http://localhost:3000/api/email/${productId}`, {});
  }
}
