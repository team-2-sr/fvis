import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';
import { UserLoginDto } from '../models/user-login-dto';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserSignUpDto } from '../models/user-sign-up-dto';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private baseUrl = 'http://localhost:3000';
  public loggedInUser$ = new BehaviorSubject<any>(this.loggedUser());
  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserLoggedIn()
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtHelper: JwtHelperService,
    private readonly router: Router
  ) {}

  registerUser(user: UserSignUpDto): Observable<any> {
    return this.http.post(`${this.baseUrl}/session/users`, user);
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  loginUser(user: UserLoginDto): Observable<any> {
    return this.http.post(`${this.baseUrl}/session`, user).pipe(
      tap(({ token }) => {
        this.isLoggedInSubject$.next(true);
      })
    );
  }

  getUserFromToken(token) {
    return this.jwtHelper.decodeToken(token);
  }

  public isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  public logout() {
    this.storage.setItem('token', '');
    this.loggedInUser$.next(null);
    this.isLoggedInSubject$.next(false);
  }

  private loggedUser(): any {
    try {
      return this.jwtHelper.decodeToken(this.storage.read('token'));
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
