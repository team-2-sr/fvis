import { AdminGuardService } from './guards/admin-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'products', loadChildren: () => import('./features/products/products.module').then(m => m.ProductsModule)},
  { path: 'admin',  loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule), canActivate: [AdminGuardService]},
  { path: 'search', loadChildren: () => import('./features/search/search.module').then(m => m.SearchModule)},
  { path: 'not-found', component: NotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
