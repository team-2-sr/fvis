import { IsString, IsNotEmpty, Length, Matches } from "class-validator";

export class LogInDto{

    @IsString()
    @IsNotEmpty()
    @Length(0,16)
    username: string;

    @Length(0,32)
    @IsNotEmpty()
    @IsString()
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z)(?=.*[a-z]).*$/)
    password: string;
}