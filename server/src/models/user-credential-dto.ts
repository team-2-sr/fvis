import { IsString, IsNotEmpty, Length, Matches } from "class-validator";

export class UserCredentialDto{
    @IsString()
    @IsNotEmpty()
    @Length(0,35)
    name: string;

    @IsString()
    @IsNotEmpty()
    @Length(0,16)
    username: string;

    @Length(0,32)
    @IsNotEmpty()
    @IsString()
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z)(?=.*[a-z]).*$/)
    password: string;

    @IsString()
    @IsNotEmpty()
    @Length(0,60)
    email: string;
}