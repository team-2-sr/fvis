import { Expose, Transform } from "class-transformer";
import { UserRole } from "./roles";

export class ShowUserDto{
    @Expose()
    public username: string;

    @Expose()
    public name: string;

    @Expose()
    public id: number;

    @Expose()
    @Transform((_,user) => UserRole[user.role])
    public role:UserRole;

    @Expose()
    public email: string;
}
