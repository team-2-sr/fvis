import { Controller, Get, HttpCode, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { SearchService } from './services/search.service';
import { ShowInventoryItemDto } from '../common/models/show-inv-item-dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('api/search')
export class SearchController {
  constructor(private readonly searchService: SearchService) {}

  @Get('/cpe')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async searchByCpe(
    @Query('cpe23Uri') cpe23Uri: string,
    @Query('title') title: string,
  ): Promise<ShowInventoryItemDto[]> {
    
    return await this.searchService.searchByCpe(cpe23Uri, title);
  }

  @Get('/cve')
 @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async searchByCveId(
    @Query('cveId') cveId: string,
  ): Promise<ShowInventoryItemDto[]> {
    return await this.searchService.searchByCveId(cveId);
  }

  @Get('/products')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async searchByProduct(
    @Query('product') product: string,
  ): Promise<ShowInventoryItemDto[]> {
    return await this.searchService.searchByProduct(product);
  }

  @Get('/products/vendor')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async searchByVendor(
    @Query('name') vendor: string,
  ): Promise<ShowInventoryItemDto[]> {
    return await this.searchService.searchByVendor(vendor);
  }
}
