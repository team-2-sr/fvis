import { Injectable } from '@nestjs/common';
import { Like, Repository, Not, IsNull } from 'typeorm';
import { Cpe } from '../../database/entities/cpe.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Inventory } from '../../database/entities/inventory.entity';
import { ShowInventoryItemDto } from '../../common/models/show-inv-item-dto';
import { plainToClass } from 'class-transformer';
import { Cve } from '../../database/entities/cve.entity';
import { ShowCpeDto } from '../../common/models/show-cpe-dto';
import { ShowCveDto } from '../../common/models/show-cve-dto';

@Injectable()
export class SearchService {
  public constructor(
    @InjectRepository(Cpe) private readonly cpeRepo: Repository<Cpe>,
    @InjectRepository(Inventory)
    private readonly inventoryRepo: Repository<Inventory>,
    @InjectRepository(Cve) private readonly cveRepo: Repository<Cve>,
  ) {}

  async searchByCpe(
    cpe23Uri: string,
    title: string,
  ): Promise<ShowInventoryItemDto[]> {
    let search;
    let products = [];

    if (cpe23Uri) {
      if (
        (cpe23Uri[0] === '*' &&
          cpe23Uri[cpe23Uri.length - 1] !== '*' &&
          cpe23Uri[cpe23Uri.length - 2] !== ':') ||
        (cpe23Uri[0] === '*' &&
          cpe23Uri[cpe23Uri.length - 1] === '*' &&
          cpe23Uri[cpe23Uri.length - 2] === ':')
      ) {
        search = { cpe23Uri: Like(`%${cpe23Uri.replace('*', '')}`) };
      } else if (
        cpe23Uri[0] !== '*' &&
        cpe23Uri[cpe23Uri.length - 1] === '*' &&
        cpe23Uri[cpe23Uri.length - 2] !== ':'
      ) {
        search = { cpe23Uri: Like(`${cpe23Uri.replace(/.$/, '')}%`) };
      } else if (
        cpe23Uri[0] === '*' &&
        cpe23Uri[cpe23Uri.length - 1] === '*' &&
        cpe23Uri[cpe23Uri.length - 2] !== ':'
      ) {
        search = {
          cpe23Uri: Like(`%${cpe23Uri.replace(/.$/, '').replace('*', '')}%`),
        };
      } else if (
        cpe23Uri[0] !== '*' &&
        cpe23Uri[cpe23Uri.length - 1] === '*' &&
        cpe23Uri[cpe23Uri.length - 2] === ':'
      ) {
        search = { cpe23Uri: Like(cpe23Uri) };
      } else if (cpe23Uri[0] !== '*' && cpe23Uri[cpe23Uri.length - 1] !== '*') {
        search = { cpe23Uri: Like(cpe23Uri) };
      }
    }

    if (title) {
      if (title[0] === '*' && title[title.length - 1] !== '*') {
        search = { title: Like(`%${title.replace('*', '')}`) };
      } else if (title[0] !== '*' && title[title.length - 1] === '*') {
        search = { title: Like(`${title.replace(/.$/, '')}%`) };
      } else if (title[0] === '*' && title[title.length - 1] === '*') {
        search = {
          title: Like(`%${title.replace(/.$/, '').replace('*', '')}%`),
        };
      } else if (title[0] !== '*' && title[title.length - 1] !== '*') {
        search = { title: Like(title) };
      }
    }

    let cpes: Cpe[] = await this.cpeRepo.find({
      where: search,
      relations: ['products'],
    });

    cpes.forEach(cpe => (products = [...products, ...cpe.products]));

    const foundProducts = (products as any).map(product => {
      product.cpe = plainToClass(ShowCpeDto, product.cpe, {
        excludeExtraneousValues: true,
      });
      product.assignedCves = (product.assignedCves as any).map(cve =>
        plainToClass(ShowCveDto, cve, { excludeExtraneousValues: true }),
      );

      return plainToClass(ShowInventoryItemDto, product, {
        excludeExtraneousValues: true,
      });
    });

    return foundProducts;
  }

  async searchByCveId(cveId: string): Promise<ShowInventoryItemDto[]> {
    let search;
    let products = [];

    if (cveId[0] === '*' && cveId[cveId.length - 1] !== '*') {
      search = { cveID: Like(`%${cveId.replace('*', '')}`) };
    } else if (cveId[0] !== '*' && cveId[cveId.length - 1] === '*') {
      search = { cveID: Like(`${cveId.replace(/.$/, '')}%`) };
    } else if (cveId[0] === '*' && cveId[cveId.length - 1] === '*') {
      search = { cveID: Like(`%${cveId.replace(/.$/, '').replace('*', '')}%`) };
    } else if (cveId[0] !== '*' && cveId[cveId.length - 1] !== '*') {
      search = { cveID: Like(cveId) };
    }

    let cves: Cve[] = await this.cveRepo.find({
      where: search,
      relations: ['products'],
    });

    cves.forEach(cve => (products = [...products, ...cve.products]));

    const foundProducts = (products as any).map(product => {
      product.cpe = plainToClass(ShowCpeDto, product.cpe, {
        excludeExtraneousValues: true,
      });
      product.assignedCves = (product.assignedCves as any).map(cve =>
        plainToClass(ShowCveDto, cve, { excludeExtraneousValues: true }),
      );

      return plainToClass(ShowInventoryItemDto, product, {
        excludeExtraneousValues: true,
      });
    });

    return foundProducts;
  }

  async searchByProduct(product: string): Promise<ShowInventoryItemDto[]> {
    let search;

    if (product[0] === '*' && product[product.length - 1] !== '*') {
      search = { product: Like(`%${product.replace('*', '')}`) };
    } else if (product[0] !== '*' && product[product.length - 1] === '*') {
      search = { product: Like(`${product.replace(/.$/, '')}%`) };
    } else if (product[0] === '*' && product[product.length - 1] === '*') {
      search = {
        product: Like(`%${product.replace(/.$/, '').replace('*', '')}%`),
      };
    } else if (product[0] !== '*' && product[product.length - 1] !== '*') {
      search = { product: Like(product) };
    }

    let products: Inventory[] = await this.inventoryRepo.find({
      where: search,
    });

    const foundProducts = (products as any).map(product => {
      product.cpe = plainToClass(ShowCpeDto, product.cpe, {
        excludeExtraneousValues: true,
      });
      product.assignedCves = (product.assignedCves as any).map(cve =>
        plainToClass(ShowCveDto, cve, { excludeExtraneousValues: true }),
      );

      return plainToClass(ShowInventoryItemDto, product, {
        excludeExtraneousValues: true,
      });
    });

    return foundProducts;
  }

  async searchByVendor(vendor: string): Promise<ShowInventoryItemDto[]> {
    let search;

    if (vendor[0] === '*' && vendor[vendor.length - 1] !== '*') {
      search = { vendor: Like(`%${vendor.replace('*', '')}`) };
    } else if (vendor[0] !== '*' && vendor[vendor.length - 1] === '*') {
      search = { vendor: Like(`${vendor.replace(/.$/, '')}%`) };
    } else if (vendor[0] === '*' && vendor[vendor.length - 1] === '*') {
      search = {
        vendor: Like(`%${vendor.replace(/.$/, '').replace('*', '')}%`),
      };
    } else if (vendor[0] !== '*' && vendor[vendor.length - 1] !== '*') {
      search = { vendor: Like(vendor) };
    }

    let products: Inventory[] = await this.inventoryRepo.find({
      where: search,
    });

    const foundProducts = (products as any).map(product => {
      product.cpe = plainToClass(ShowCpeDto, product.cpe, {
        excludeExtraneousValues: true,
      });
      product.assignedCves = (product.assignedCves as any).map(cve =>
        plainToClass(ShowCveDto, cve, { excludeExtraneousValues: true }),
      );

      return plainToClass(ShowInventoryItemDto, product, {
        excludeExtraneousValues: true,
      });
    });

    return foundProducts;
  }
}
