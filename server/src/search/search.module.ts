import { Module } from '@nestjs/common';
import { SearchController } from './search.controller';
import { SearchService } from './services/search.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Inventory } from '../database/entities/inventory.entity';
import { Cpe } from '../database/entities/cpe.entity';
import { Cve } from '../database/entities/cve.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Inventory, Cpe, Cve])],
  controllers: [SearchController],
  providers: [SearchService]
})
export class SearchModule {}
