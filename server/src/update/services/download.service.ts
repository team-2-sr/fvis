import { Subscription } from 'rxjs';
import { Injectable, HttpService } from "@nestjs/common";
const fs = require('fs');


@Injectable()
export class DownloadService {
    constructor(private readonly httpService: HttpService) {}

    public async download(url: string, filePath: string): Promise<void> {
        let subscription: Subscription;
    
        const writer = fs.createWriteStream(filePath)
    
        // This downloads and saves the zip file.
        await new Promise((resolve, reject) => {
            subscription = this.httpService.get(url, {responseType: 'stream'})
                .subscribe(res => {
                    res.data.pipe(writer);
                    writer.on('finish', resolve)
                    writer.on('error', reject)
    
                    },
                    (error) => reject(error)
                );
        });
        
        subscription.unsubscribe();
        return;
    }
}










// public async download(url: string, filePath: string): Promise<void> {

//     const writer = fs.createWriteStream(filePath)

//     // This downloads and saves the zip file.
//     await new Promise((resolve, reject) => {
//         https.get(url, function(response) {
//             response.pipe(writer);
//             writer.on('finish', function() {
//             writer.close((err) =>{
//                 if(err) {
//                     console.log(err);
//                 }
                
//                 resolve();
//             } );
//             });
//             writer.on('error', (err) => {
//                 console.log(err);
//                 reject();
                
//             });
//           });
//     });

//     return;
// }