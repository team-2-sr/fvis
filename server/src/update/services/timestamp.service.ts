
import { Repository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { Timestamp } from '../../database/entities/timestamp.entity';
import { Feeds } from '../../common/enum/feeds';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TimestampService {

    public constructor(
        @InjectRepository(Timestamp) private readonly timeRepo: Repository<Timestamp>) {}

    public async updateTime(id: Feeds, time: string): Promise<void> {
        const foundStamp = await this.timeRepo.findOne(id);
        if(!foundStamp) {
            const createdStamp = this.timeRepo.create({id, updatedOn: time });
            await this.timeRepo.save(createdStamp);
            return;
        }

        foundStamp.updatedOn = time;
        await this.timeRepo.save(foundStamp);
    }

    public async getCveUpdateTime(options: {seeding: boolean}): Promise<string> {
        const id = options.seeding ? Feeds.CVE_SEEDING : Feeds.CVE;

        const foundStamp = await this.timeRepo.findOne(id);
        if(!foundStamp) {
            return;
        }

        return foundStamp.updatedOn;
    }

    public async getCpeUpdateTime(options: {seeding: boolean}): Promise<string> {
        const id = options.seeding ? Feeds.CPE_SEEDING : Feeds.CPE;

        const foundStamp = await this.timeRepo.findOne(id);
        if(!foundStamp) {
            return;
        }

        return foundStamp.updatedOn;
    }
}