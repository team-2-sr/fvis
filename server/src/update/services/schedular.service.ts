import { Injectable } from '@nestjs/common';
import {SchedulerRegistry, Cron, CronExpression, Interval} from '@nestjs/schedule';
import { UpdateName } from '../../common/enum/update-name';
import { UpdateCpesService } from './update-cpes.service';
import { UpdateCvesService } from './update-cves.service';
const CronTime = require('cron').CronTime;

@Injectable()
export class SchedularService {
    constructor(private schedulerRegistry: SchedulerRegistry,
                private readonly updateCpesService: UpdateCpesService,
                private readonly updateCvesService: UpdateCvesService) {}

    setCpeUpdate(seconds: number): {message: string} {
            
            const intervals = this.schedulerRegistry.getIntervals();
            
            if (intervals.includes('cpe-update')) {
                this.schedulerRegistry.deleteInterval('cpe-update');
            }

            const callback = () => {
                console.log('CPE scheduled updated called');
                this.updateCpesService.updateCpes();
            };
          
            const interval = setInterval(callback, seconds);
            this.schedulerRegistry.addInterval('cpe-update', interval);

            return {message: 'CPEs update set!'};
    }

    setCveUpdate(seconds: number): {message: string} {
            
        const intervals = this.schedulerRegistry.getIntervals();
        
        if (intervals.includes('cve-update')) {
            this.schedulerRegistry.deleteInterval('cve-update');
        }

        const callback = () => {
            console.log('CVE scheduled updated called');
            this.updateCvesService.updateBothCves()
        };
      
        const interval = setInterval(callback, seconds);
        this.schedulerRegistry.addInterval('cve-update', interval);

        return {message: 'CVEs update set!'};
}

    deleteUpdate(name: UpdateName) {
        const intervals = this.schedulerRegistry.getIntervals();
            
        if (intervals.includes(name)) {
            this.schedulerRegistry.deleteInterval(name);
        }

        return {message: `${name} update deleted`};
    }

}
