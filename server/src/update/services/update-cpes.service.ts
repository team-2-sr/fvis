import { InjectRepository } from "@nestjs/typeorm";
import { Cpe } from "../../database/entities/cpe.entity";
import { Repository } from "typeorm";
import { TimestampService } from "./timestamp.service";
import { DownloadService } from "./download.service";
import { UnzipService } from "./unzip.service";
import { Injectable } from "@nestjs/common";
import { TrackingSystemError } from "../../common/exceptions/tracking-system.error";
import readline = require('readline');
import { Feeds } from "../../common/enum/feeds";
const fs = require('fs');

@Injectable()
export class UpdateCpesService {

    constructor(
        @InjectRepository(Cpe) private readonly cpeRepo: Repository<Cpe>,
        private readonly timestampService: TimestampService,
        private readonly downloadService: DownloadService,
        private readonly unzipService: UnzipService
    ) {}

    async initialCpesSeed(): Promise<void> {
        const extraxtedFilePath = './src/update/feeds/official-cpe-dictionary_v2.3.xml';

        console.log('Cpes seed started!');
        await this.timestampService.updateTime(Feeds.CPE_SEEDING, Date.now().toString());

        const fileStream = fs.createReadStream(extraxtedFilePath);
        let timestamp: string;
        let title = '', cpe23Uri = '', vendor = '', product = '', version = '', update = '', counter = 0, counterAll = 0, chunkCpes = [], depricated = '';
    
        const rl = readline.createInterface({
            input: fileStream,
          });
          
        for await (const line of rl) {
            if (line.includes('<timestamp>')) {
                const startIndex =  line.indexOf('>') + 1;
                const endIndex = line.indexOf('<', startIndex);
                timestamp = line.substring(startIndex, endIndex);
            }

            if (line.includes(`<cpe-item`) && line.includes(`deprecated=`) ) {
                const startIndex =  line.indexOf('deprecated=') + 12;
                const endIndex = line.indexOf(' ', startIndex) - 1;
                depricated = line.substring(startIndex, endIndex);
            }

            if (line.includes('<title ')) {
                const startIndex =  line.indexOf('>') + 1;
                const endIndex = line.indexOf('<', startIndex);
                title = line.substring(startIndex, endIndex);
            }
            if (line.includes('<cpe-23:cpe23-item')) {
                const startIndex = line.indexOf('=') + 2;
                const endIndex = line.indexOf('"', startIndex);
                cpe23Uri = line.substring(startIndex, endIndex);
                vendor = cpe23Uri.split(':')[3];
                product = cpe23Uri.split(':')[4];
                version = cpe23Uri.split(':')[5];
                update = cpe23Uri.split(':')[6];
               // edition = cpe23Uri.split(':')[7];
            }
        
            if (line.includes(`</cpe-item>`)) {
                const type = cpe23Uri.split(':')[2];
                
                if(type === 'a') {
                    if (depricated !== 'true') {
                        const createdCpe = this.cpeRepo.create({title, vendor, product, version, update, cpe23Uri});
                        createdCpe.products = [];
        
                        // This is done only to limit database calls and spped up the loading.
                        chunkCpes.push(createdCpe);
                        counter++;
                    }
                }
    
                if (counter >= 1000) {
                    await this.cpeRepo.save(chunkCpes);
                    chunkCpes = [];
                    counter = 0;
                    counterAll = counterAll + 1000;
                }
    
                title = '';
                cpe23Uri = '';
                vendor = '';
                product = '';
                version = '';
                update = '';
                depricated = '';
            }
        }

        if(chunkCpes.length > 0) {
            await this.cpeRepo.save(chunkCpes);
            counterAll = counterAll + chunkCpes.length;
        }

        await this.timestampService.updateTime(Feeds.CPE, timestamp);
        await this.timestampService.updateTime(Feeds.CPE_SEEDING, '');
    
        console.log('Cpes seed completed!');
        console.log(`${counterAll} CPEs seeded`);
        
        // return {message: 'CPEs seeded'};
    }

    async downloadAndUnzipLatestCpeFeed(): Promise<void> {
        const zipFilePath = './src/update/feeds/official-cpe-dictionary_v2.3.xml.zip';
        const insideFilePath = 'official-cpe-dictionary_v2.3.xml'
        const extraxtedFilePath = './src/update/feeds/official-cpe-dictionary_v2.3.xml';
        const url = 'https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.zip';

        const cpeTimestamp = await this.timestampService.getCpeUpdateTime({seeding:false})

        if (cpeTimestamp) {
            throw new TrackingSystemError('Cpes already seeded!', 403);  
        }

        try {
            await this.downloadService.download(url, zipFilePath);
        } catch (err) {
            console.log(err);
            
            throw new TrackingSystemError('NVD feed problem, please try again later', 500);  
        } 
        
        await this.unzipService.extractFileFromZip(zipFilePath, insideFilePath, extraxtedFilePath);
    }

    async updateCpes(): Promise<{message: string}> {
        const zipFilePath = './src/update/feeds/official-cpe-dictionary_v2.3.xml.zip';
        const insideFilePath = 'official-cpe-dictionary_v2.3.xml'
        const extraxtedFilePath = './src/update/feeds/official-cpe-dictionary_v2.3.xml';
        const url = 'https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.zip';
    
        const cpeTimestamp = await this.timestampService.getCpeUpdateTime({seeding:false})
    
        if (!cpeTimestamp) {
            throw new TrackingSystemError('Cpes not seeded yet, please start initial seed', 403);  
        }
    
        try {
            await this.downloadService.download(url, zipFilePath);
        } catch (err) {
            throw new TrackingSystemError('NVD feed problem, please try again later', 500);  
        }

        await this.downloadAndUnzipLatestCpeFeed();
    
        console.log('Cpes update started!');
    
        const fileStream = fs.createReadStream(extraxtedFilePath);
        let timestamp: string;
        let title = '', cpe23Uri = '', vendor = '', product = '', version = '',
            update = '', counter = 0, counterAll = 0, chunkCpes = [], depricated = '', depricatedBy = '', depricatedOn = '';
    
        const rl = readline.createInterface({
            input: fileStream,
            });
            
        for await (const line of rl) {
            if (line.includes('<timestamp>')) {
                const startIndex =  line.indexOf('>') + 1;
                const endIndex = line.indexOf('<', startIndex);
                timestamp = line.substring(startIndex, endIndex);
            }
    
            if (line.includes(`<cpe-item`) && line.includes(`deprecated=`) ) {
                const startIndex =  line.indexOf('deprecated=') + 12;
                const endIndex = line.indexOf(' ', startIndex) - 1;
                depricated = line.substring(startIndex, endIndex);
            }
    
            if (line.includes('<title ')) {
                const startIndex =  line.indexOf('>') + 1;
                const endIndex = line.indexOf('<', startIndex);
                title = line.substring(startIndex, endIndex);
            }
            if (line.includes('<cpe-23:cpe23-item')) {
                const startIndex = line.indexOf('=') + 2;
                const endIndex = line.indexOf('"', startIndex);
                cpe23Uri = line.substring(startIndex, endIndex);
                vendor = cpe23Uri.split(':')[3];
                product = cpe23Uri.split(':')[4];
                version = cpe23Uri.split(':')[5];
                update = cpe23Uri.split(':')[6];
                // edition = cpe23Uri.split(':')[7];
            }
    
            if (line.includes('<cpe-23:deprecation')) {
                const startIndex = line.indexOf('=') + 2;
                const endIndex = line.indexOf('"', startIndex);
                depricatedOn = line.substring(startIndex, endIndex);
            }
    
            if (line.includes('<cpe-23:deprecated-by')) {
                const startIndex = line.indexOf('=') + 2;
                const endIndex = line.indexOf('"', startIndex);
                depricatedBy = line.substring(startIndex, endIndex);
            }
        
            if (line.includes(`</cpe-item>`)) {
                const type = cpe23Uri.split(':')[2];
    
                if(type === 'a') {
                    const foundCpe = await this.cpeRepo.findOne({
                        where: {cpe23Uri},
                        relations: ['products']
                    });
    
                    if (foundCpe) {                                               
                        if (depricated === 'true') {
                            if (foundCpe.deprecated === false && foundCpe.products.length === 0) {
                                foundCpe.deprecated = true;
                                // foundCpe.depricatedBy = depricatedBy;
                                // foundCpe.depricatedOn = depricatedOn;
                                await this.cpeRepo.save(foundCpe);                                
                            }
                        }
                    } else {
                        if (depricated !== 'true') {
                            const createdCpe = this.cpeRepo.create({title, vendor, product, version, update, cpe23Uri});
                            createdCpe.products = [];
            
                            // This is done only to limit database calls and speed up the loading.
                            chunkCpes.push(createdCpe);
                            counter++;
                            counterAll++;
                        }
                    }
                }
    
                if (counter >= 1000) {
                    await this.cpeRepo.save(chunkCpes);
                    chunkCpes = [];
                    counter = 0;
                }
    
                title = '';
                cpe23Uri = '';
                vendor = '';
                product = '';
                version = '';
                update = '';
                depricated = '';
                depricatedBy = '';
            }
        }
    
        if(chunkCpes.length > 0) {
            counterAll = counterAll + chunkCpes.length;
            await this.cpeRepo.save(chunkCpes);
        }
    
        await this.timestampService.updateTime(Feeds.CPE, timestamp);
    
        console.log('Cpes seed update completed!');
        console.log(`${counterAll} new CPEs added`);
    
        return {message: 'Cpes updated'};
    }
}
