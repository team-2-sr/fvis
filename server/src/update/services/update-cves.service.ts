import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Repository } from 'typeorm';
import { Cve } from '../../database/entities/cve.entity';
import { CveCpe } from '../../database/entities/cve-cpe.entity';
import { Feeds } from '../../common/enum/feeds';
import { UnzipService } from './unzip.service';
import { DownloadService } from './download.service';
import { InjectRepository } from '@nestjs/typeorm';
import { TimestampService } from './timestamp.service';
import { TrackingSystemError } from '../../common/exceptions/tracking-system.error';
const Fs = require('fs'); 

@Injectable()
export class UpdateCvesService {

    constructor(
        @InjectRepository(Cve) private readonly cveRepo: Repository<Cve>,
        @InjectRepository(CveCpe) private readonly cveCpeRepo: Repository<CveCpe>,
        private readonly timestampService: TimestampService,
        private readonly downloadService: DownloadService,
        private readonly unzipService: UnzipService
    ) {}

    async downloadAndUnzipLatestCveFeed(): Promise<void> {
        const zipFilePath = './src/update/feeds/nvdcve-1.1-2020.json.zip';
        const insideFilePath = 'nvdcve-1.1-2020.json'
        const extraxtedFilePath = './src/update/feeds/nvdcve-1.1-2020.json';
        const url = 'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-2020.json.zip';

        const cveTimestamp = await this.timestampService.getCveUpdateTime({seeding:false});

        if (cveTimestamp) {
            throw new TrackingSystemError('Cves already seeded!', 403);  
        }

        try {
            await this.downloadService.download(url, zipFilePath);
        } catch (err) {
            throw new TrackingSystemError('NVD feed problem, please try again later', 500);  
        } 
        
        await this.unzipService.extractFileFromZip(zipFilePath, insideFilePath, extraxtedFilePath);
    }

    async initialCvesSeed(): Promise<{message: string}> {
        const extraxtedFilePath = './src/update/feeds/nvdcve-1.1-2020.json';
  
        await new Promise(async resolve => {
            const self = this;

            console.log('CVEs seed started');
            await this.timestampService.updateTime(Feeds.CVE_SEEDING, Date.now().toString());
            
            Fs.readFile(extraxtedFilePath, {}, async function(err, data) {
                const result = JSON.parse(data);
                let count = 0;
                let countTotal = 0

                for (const cveItem of result['CVE_Items']) {
                    const itemMeta = self.extractCveItemMeta(cveItem);    
                    let cpes: CveCpe[] = [];

                    const cveCreted = self.cveRepo.create({cveID: itemMeta.cveID});
                    cveCreted.description = itemMeta.description;
                    cveCreted.severity = itemMeta.severity;
                    cveCreted.lastModifiedDate = itemMeta.lastModifiedDate;
                    cveCreted.products = [];
                    const savedCve: Cve = await self.cveRepo.save(cveCreted);
                    
                    for (const node of itemMeta.nodes ) {
                        cpes = [...cpes, ...(await self.createNodeCpes(node, savedCve))];
                    }
                    //check ToDO
                    if (cpes.length > 0) {
                        count++;
                    }
                    countTotal++;
                }
                // This saves the time stamp of this seed;
                await self.timestampService.updateTime(Feeds.CVE, result['CVE_data_timestamp']);
                await self.timestampService.updateTime(Feeds.CVE_SEEDING, '');
        
                console.log(`Created CVE's with assigned cpes`, count);
                console.log(`Created CVE's total`, countTotal);
                resolve();
            });
       });
       
       return {message: 'Cves recent updated'}
    }

    // These are to be scheduled only
    async updateBothCves(): Promise<{message: string}> {

        console.log('Cves update started!');

        await this.updateRecentCves();
        console.log('Cves recent updated');

        await this.updateModifiedCves();
        console.log('Cves modified updated');
        
        return {message: 'Cves recent and modified updated'}
    }

    // These are to be scheduled only
    private async updateModifiedCves(): Promise<{message: string}> {
        const zipFilePath = './src/update/feeds/nvdcve-1.1-modified.json.zip';
        const insideFilePath = 'nvdcve-1.1-modified.json'
        const extraxtedFilePath = './src/update/feeds/nvdcve-1.1-modified.json';
        const url = 'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-modified.json.zip';

        const cveTimestamp = await this.timestampService.getCveUpdateTime({seeding:false});

        if (!cveTimestamp) {
            throw new TrackingSystemError('Cves not seeded yet, please start initial seed', 403);  
        }

        try {
            await this.downloadService.download(url, zipFilePath);
        } catch (err) {
            throw new TrackingSystemError('NVD feed problem, please try again later', 500);  
        } 
        
        await this.unzipService.extractFileFromZip(zipFilePath, insideFilePath, extraxtedFilePath);

        await new Promise(resolve => {
            const self = this;

            Fs.readFile(extraxtedFilePath, {}, async function(err, data) {
                const result = JSON.parse(data);
                let count = 0;

                for (const cveItem of result['CVE_Items']) {
                    const itemMeta = self.extractCveItemMeta(cveItem);
    
                    let cpes: CveCpe[] = [];

                    const foundCve = await self.cveRepo.findOne({
                        where: {cveID: itemMeta.cveID}
                    })

                    if (!foundCve) {
                        console.log('Not found', itemMeta.cveID);
                        continue;
                    }
            
                    foundCve.description = itemMeta.description;
                    foundCve.severity = itemMeta.severity;
                    foundCve.lastModifiedDate = itemMeta.lastModifiedDate;
                    const savedCve: Cve = await self.cveRepo.save(foundCve);

                    for (const node of itemMeta.nodes ) {
                        cpes = [...cpes, ...(await self.createNodeCpes(node, savedCve))];
                    }
                    //check ToDO
                    if (cpes.length > 0) {
                        count++;
                    }
                }
                // This saves the time stamp of this seed;
                await self.timestampService.updateTime(Feeds.CVE, result['CVE_data_timestamp']);
        
                console.log(`Updated CVE's with assigned cpes`, count);
                resolve();
            });
       });
       
       return {message: 'Cves modified updated'}
    }

     // These are to be scheduled only
    private async updateRecentCves(): Promise<{message: string}> {
        const zipFilePath = './src/update/feeds/nvdcve-1.1-recent.json.zip';
        const insideFilePath = 'nvdcve-1.1-recent.json'
        const extraxtedFilePath = './src/update/feeds/nvdcve-1.1-recent.json';
        const url = 'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-recent.json.zip';

        const cveTimestamp = await this.timestampService.getCveUpdateTime({seeding:false});

        if (!cveTimestamp) {
            throw new TrackingSystemError('Cves not seeded yet, please start initial seed', 403);  
        }

        try {
            await this.downloadService.download(url, zipFilePath);
        } catch (err) {
            throw new TrackingSystemError('NVD feed problem, please try again later', 500);  
        } 
        

        await this.unzipService.extractFileFromZip(zipFilePath, insideFilePath, extraxtedFilePath);

        await new Promise(resolve => {
            const self = this;

            Fs.readFile(extraxtedFilePath, {}, async function(err, data) {
                const result = JSON.parse(data);
                let count = 0;

                for (const cveItem of result['CVE_Items']) {
                    const itemMeta = self.extractCveItemMeta(cveItem);
                    let cpes: CveCpe[] = [];

                    const foundCve = await self.cveRepo.findOne({
                        where: {cveID: itemMeta.cveID}
                    })

                    let savedCve: Cve;

                    if (foundCve) {
                        console.log('found', itemMeta.cveID);
         
                        foundCve.description = itemMeta.description;
                        foundCve.severity = itemMeta.severity;
                        foundCve.lastModifiedDate = itemMeta.lastModifiedDate;
                        savedCve = await self.cveRepo.save(foundCve);
                    } else {
                        const cveCreted = self.cveRepo.create({cveID: itemMeta.cveID});
                        cveCreted.description = itemMeta.description;
                        cveCreted.severity = itemMeta.severity;
                        cveCreted.lastModifiedDate = itemMeta.lastModifiedDate;
                        cveCreted.products = [];
                        savedCve = await self.cveRepo.save(cveCreted);
                    }
                    

                    for (const node of itemMeta.nodes ) {
                        cpes = [...cpes, ...(await self.createNodeCpes(node, savedCve))];
                    }
                    //check ToDO
                    if (cpes.length > 0) {
                        count++;
                    }
                }
                // This saves the time stamp of this seed;
                await self.timestampService.updateTime(Feeds.CVE, result['CVE_data_timestamp']);
        
                console.log(`Created CVE's with assigned cpes`, count);
                resolve();
            });
       });
       
       return {message: 'Cves recent updated'}
    }

    private async createNodeCpes(node, cve: Cve): Promise<CveCpe[]> {
        if(!node['children']) {
            return await this.updateCpeMatches(node['cpe_match'], cve);
        }
        
        let cpeMatches: CveCpe[] = [];

        for (const child of node['children'] ) {
            cpeMatches = [...cpeMatches, ...(await this.createNodeCpes(child, cve))]
        };

       return cpeMatches;
    }

    private async updateCpeMatches(nodeCpeMatches,  cve: Cve): Promise<CveCpe[]> {
        const cpesSaved: CveCpe[] = [];
        
        for (const cpe of nodeCpeMatches) {
            const type = cpe.cpe23Uri.split(':')[2];

            if (type === 'a' && cpe.vulnerable === true) {
                const foundCpe = await this.cveCpeRepo.findOne({
                    where: {cpe23Uri: cpe.cpe23Uri,
                        vulnerable: true,
                        versionEndIncluding: cpe.versionEndIncluding ? cpe.versionEndIncluding : null,
                        versionEndExcluding: cpe.versionEndExcluding ? cpe.versionEndExcluding : null,
                        versionStartIncluding: cpe.versionStartIncluding ? cpe.versionStartIncluding : null,
                        versionStartExcluding: cpe.versionStartExcluding ? cpe.versionStartExcluding : null
                    },
                    relations: ['cves']
                })

                if(!foundCpe) {
                    const cpeCreated = this.cveCpeRepo.create({
                        vulnerable: cpe.vulnerable,
                        cpe23Uri: cpe.cpe23Uri,
                    });
        
                    if(cpe.versionEndIncluding) {
                        cpeCreated.versionEndIncluding = cpe.versionEndIncluding;
                    }
                    if(cpe.versionEndExcluding) {
                        cpeCreated.versionEndExcluding = cpe.versionEndExcluding;
                    }
                    if(cpe.versionStartIncluding) {
                        cpeCreated.versionStartIncluding = cpe.versionStartIncluding;
                    }
                    if(cpe.versionStartExcluding) {
                        cpeCreated.versionStartExcluding = cpe.versionStartExcluding;
                    }

                    cpeCreated.cves = [cve];
    
                    const savedCpe: CveCpe = await this.cveCpeRepo.save(cpeCreated);
                    cpesSaved.push(savedCpe);
                } else if (foundCpe) {
                    const cpeCveIds = foundCpe.cves.map(cve => cve.id);
                    if (!cpeCveIds.includes(cve.id)) {
                        foundCpe.cves = [...foundCpe.cves, cve];
                        await this.cveCpeRepo.save(foundCpe);
                    }
               
                    cpesSaved.push(foundCpe)
                }
            }
        };
        return cpesSaved;
    }

    private extractCveItemMeta(cveItem: any) {
        const cveID = cveItem['cve']['CVE_data_meta']['ID'];
        const description = cveItem['cve']['description']['description_data'][0]['value'];
        const severity = cveItem['impact']['baseMetricV2'] 
            ? cveItem['impact']['baseMetricV2']['severity'] 
            : null;
        const lastModifiedDate = cveItem['lastModifiedDate'];
        const nodes = cveItem['configurations']['nodes'];

        return {
            cveID,
            description,
            severity,
            lastModifiedDate,
            nodes
        }
    }
}


// async repopulateInitialCves(): Promise<{message: string}> {
//     const extraxtedFilePath = './src/update/feeds/nvdcve-1.1-2020.json';

//     console.log('CVEs repopulating started');
//     await this.timestampService.updateTime(Feeds.CVE_SEEDING, Date.now().toString());

//     await new Promise(resolve => {
//         const self = this;

//         Fs.readFile(extraxtedFilePath, {}, async function(err, data) {
//             const result = JSON.parse(data);
//             let count = 0;
//             let countTotal = 0

//             for (const cveItem of result['CVE_Items']) {
//                 const itemMeta = self.extractCveItemMeta(cveItem);        
//                 let cpes: CveCpe[] = [];

//                 const foundCve = await self.cveRepo.findOne({
//                     where: {cveID: itemMeta.cveID}
//                 })

//                 let savedCve: Cve;

//                 if (foundCve) {
//                     console.log('found', itemMeta.cveID);
     
//                     foundCve.description = itemMeta.description;
//                     foundCve.severity = itemMeta.severity;
//                     foundCve.lastModifiedDate = itemMeta.lastModifiedDate;
//                     savedCve = await self.cveRepo.save(foundCve);
//                 } else {
//                     const cveCreted = self.cveRepo.create({cveID: itemMeta.cveID});
//                     cveCreted.description = itemMeta.description;
//                     cveCreted.severity = itemMeta.severity;
//                     cveCreted.lastModifiedDate = itemMeta.lastModifiedDate;
//                     cveCreted.products = [];
//                     savedCve = await self.cveRepo.save(cveCreted);
//                 }
                

//                 for (const node of itemMeta.nodes ) {
//                     cpes = [...cpes, ...(await self.createNodeCpes(node, savedCve))];
//                 }
//                 //check ToDO
//                 if (cpes.length > 0) {
//                     count++;
//                 }
//                 countTotal++;
//             }
//             // This saves the time stamp of this seed;
//             await self.timestampService.updateTime(Feeds.CVE, result['CVE_data_timestamp']);
//             await self.timestampService.updateTime(Feeds.CVE_SEEDING, '');
    
//             console.log(`Created CVE's with assigned cpes`, count);
//             console.log(`Created CVE's total`, countTotal);
//             resolve();
//         });
//    });
   
//    return {message: 'Cves recent updated'}
// }

