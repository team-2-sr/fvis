import { Injectable } from "@nestjs/common";
import StreamZip = require('node-stream-zip');


@Injectable()
export class UnzipService {
    
    public async extractFileFromZip(zipFilePath: string, insideFilePath:string, extraxtedFilePath: string): Promise<void> {

        await new Promise((resolve, reject) => {
            const zip = new StreamZip({
                file: zipFilePath,
                storeEntries: true
            });

            zip.on('ready', () => {
                zip.extract(insideFilePath, extraxtedFilePath, err => {
                    console.log(err ? 'Extract error' : 'Extracted');
                    zip.close();
                    resolve();
                });
            });
        });
        

        return;
   }
}