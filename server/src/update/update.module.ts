import { Module, HttpModule } from '@nestjs/common';
import { UpdateController } from './update.controller';
import { UpdateCvesService } from './services/update-cves.service';
import { DownloadService } from './services/download.service';
import { UnzipService } from './services/unzip.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cve } from '../database/entities/cve.entity';
import { CveCpe } from '../database/entities/cve-cpe.entity';
import { Timestamp } from '../database/entities/timestamp.entity';
import { TimestampService } from './services/timestamp.service';
import { UpdateCpesService } from './services/update-cpes.service';
import { Cpe } from '../database/entities/cpe.entity';
import { SchedularService } from './services/schedular.service';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  controllers: [UpdateController],
  imports: [
    TypeOrmModule.forFeature([Cve, CveCpe, Timestamp, Cpe]),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    ScheduleModule.forRoot()
  ],
  providers: [UpdateCvesService, DownloadService, UnzipService, TimestampService, UpdateCpesService, SchedularService]
})
export class UpdateModule {}
