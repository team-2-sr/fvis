import { Controller, Get, HttpCode, HttpStatus, Put, Post, UseGuards, ParseBoolPipe, Query, ParseIntPipe, Delete } from '@nestjs/common';
import { UpdateCvesService } from './services/update-cves.service';
import { UpdateCpesService } from './services/update-cpes.service';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { TimestampService } from './services/timestamp.service';
import { SchedularService } from './services/schedular.service';
import { UpdateName } from '../common/enum/update-name';

@Controller('api/update')
// @UseGuards( AuthGuard('jwt'), AdminGuard)
export class UpdateController {

    constructor (private readonly updateCvesService: UpdateCvesService,
                private readonly updateCpesService: UpdateCpesService,
                private readonly timestampService: TimestampService,
                private readonly schedularService: SchedularService) {}

    @Post('cve')
    @HttpCode(HttpStatus.OK)
    public async initialCvesSeed(
        ): Promise<{message: string}> {

        await this.updateCvesService.downloadAndUnzipLatestCveFeed();

        this.updateCvesService.initialCvesSeed();

        return {message:'CVEs seed started'};
    }

    @Post('cpe')
    @HttpCode(HttpStatus.OK)
    public async initialCpesSeed(
        ): Promise<{message: string}> {

            await this.updateCpesService.downloadAndUnzipLatestCpeFeed();

            this.updateCpesService.initialCpesSeed();

            return {message:'CPEs seed started'}
    }

    @Put('cpe/updatetime')
    @HttpCode(HttpStatus.OK)
    public async setCpeUpdateTime(
        @Query('time', new ParseIntPipe()) time: number,
        ): Promise<{message: string}> {

        return this.schedularService.setCpeUpdate(time);
    }

    @Delete('cpe/update')
    @HttpCode(HttpStatus.OK)
    public async deleteCpeUpdateTime(
        ): Promise<{message: string}> {

        return this.schedularService.deleteUpdate(UpdateName.CPE_UPDATE);
    }

    @Put('cve/updatetime')
    @HttpCode(HttpStatus.OK)
    public async setCveUpdateTime(
        @Query('time', new ParseIntPipe()) time: number,
        ): Promise<{message: string}> {

        return this.schedularService.setCveUpdate(time);
    }

    @Delete('cve/update')
    @HttpCode(HttpStatus.OK)
    public async deleteCveUpdateTime(
        ): Promise<{message: string}> {

        return this.schedularService.deleteUpdate(UpdateName.CVE_UPDATE);
    }

    @Get('cpe/timestamp')
    @HttpCode(HttpStatus.OK)
    public async getCpeUpdateTime(
        @Query('seeding', new ParseBoolPipe()) seeding: boolean,
        ): Promise<string> {

        return await this.timestampService.getCpeUpdateTime({seeding});
    }

    @Get('cve/timestamp')
    @HttpCode(HttpStatus.OK)
    public async getCveUpdateTime(
        @Query('seeding', new ParseBoolPipe()) seeding: boolean,
        ): Promise<string> {

        return await this.timestampService.getCveUpdateTime({seeding});
    }
}



// @Put('cve/deep')
// @HttpCode(HttpStatus.OK)
// public async updateInitialCves(
//     ): Promise<{message: string}> {

//     await this.updateCvesService.downloadAndUnzipLatestCveFeed();

//     this.updateCvesService.repopulateInitialCves();

//     return {message:'CVEs repopulate started'};
// }
