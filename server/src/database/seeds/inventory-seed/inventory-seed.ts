import { createConnection, Connection, Repository } from 'typeorm';
import { Inventory } from './../../entities/inventory.entity';

const fs = require('fs');

export const seedInventory = async (inputFile: string, inventoryRepo: Repository<Inventory>): Promise<void>  => {
    await new Promise(resolve => {

        fs.readFile(inputFile, async function(err, data) {
            const result = JSON.parse(data);
            
            for(const item of result) {
                const createdItem = inventoryRepo.create({
                    product: item.product,
                    vendor: item.vendor,
                    version: item.version
                });
                createdItem.assignedCves = [];

                await inventoryRepo.save(createdItem);
            }

            resolve()
        });
    });
}

const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();
    const inventoryRepo = connection.getRepository(Inventory);
    const inputFilePath = './src/database/seeds/inventory-seed/feed/inventory.json';

    await seedInventory(inputFilePath, inventoryRepo);

    await connection.close();
            
    console.log('completed!');
  };
  
seed().catch(console.error);