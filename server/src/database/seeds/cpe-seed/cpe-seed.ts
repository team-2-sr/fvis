import fs = require('fs');
import readline = require('readline');
import { createConnection, Repository } from 'typeorm';
import { Cpe } from '../../entities/cpe.entity';
import { UpdateTime } from '../timestamp/updateTime';
import { Feeds } from '../../../common/enum/feeds';

export class CpeSeeder {
    constructor(private readonly cpeRepo: Repository<Cpe>,
                private readonly updateTime: UpdateTime) {}
    
    public async seedCpes(inputFile: string): Promise<void> {
        const fileStream = fs.createReadStream(inputFile);
        let timestamp: string;
        let title = '', cpe23Uri = '', vendor = '', product = '', version = '', update = '', counter = 0, chunkCpes = [];
    
        const rl = readline.createInterface({
            input: fileStream,
          });
          
        for await (const line of rl) {
            if (line.includes('<timestamp>')) {
                const startIndex =  line.indexOf('>') + 1;
                const endIndex = line.indexOf('<', startIndex);
                timestamp = line.substring(startIndex, endIndex);
            }

            if (line.includes('<title ')) {
                const startIndex =  line.indexOf('>') + 1;
                const endIndex = line.indexOf('<', startIndex);
                title = line.substring(startIndex, endIndex);
            }
            if (line.includes('<cpe-23:cpe23-item')) {
                const startIndex = line.indexOf('=') + 2;
                const endIndex = line.indexOf('"/>');
                cpe23Uri = line.substring(startIndex, endIndex);
                vendor = cpe23Uri.split(':')[3];
                product = cpe23Uri.split(':')[4];
                version = cpe23Uri.split(':')[5];
                update = cpe23Uri.split(':')[6];
               // edition = cpe23Uri.split(':')[7];
            }
        
            if (line.includes(`</cpe-item>`)) {
                const type = cpe23Uri.split(':')[2];
                
                if(type === 'a') {
                    const createdCpe = this.cpeRepo.create({title, vendor, product, version, update, cpe23Uri});
                    createdCpe.products = [];
    
                    // This is done only to limit database calls and spped up the loading.
                    chunkCpes.push(createdCpe);
                    counter++;
                }
    
                if (counter >= 1000) {
                    await this.cpeRepo.save(chunkCpes);
                    chunkCpes = [];
                    counter = 0;
                }
    
                title = '';
                cpe23Uri = '';
                vendor = '';
                product = '';
                version = '';
                update = '';
            }
        }

        if(chunkCpes.length > 0) {
            await this.cpeRepo.save(chunkCpes);
        }

        await this.updateTime.timeStamp(Feeds.CPE, timestamp);
    }
}

const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();
    const cpeRepo = connection.getRepository(Cpe);
    const updateTime = new UpdateTime(connection);
    const inputFilePath = './src/database/seeds/cpe-seed/feed/official-cpe-dictionary_v2.3.xml';

    const cpeSeeder = new CpeSeeder(cpeRepo, updateTime)

    await cpeSeeder.seedCpes(inputFilePath);

    await connection.close();
            
    console.log('completed!');
  };
  
seed().catch(console.error);




// Tests
// const foundCpe = await cpeRepo.findOne(964);
// const cveRepo = connection.getRepository(Cve);
// const cve = await cveRepo.findOne(1);
// foundCpe.assignedCves = [cve];
// await cpeRepo.save(foundCpe);
// const updatedCve = await cveRepo.findOne({where: {id: 1}, relations: ['assignedCpes']});
// console.log(updatedCve);

// const foundCpe = await cpeRepo.find({
//     where: {product: 'git', vendor: 'git-scm', version: '1'}
// })

// console.log(foundCpe);

// const foundCpe = await cpeRepo.find()
    
// console.log(foundCpe.length);