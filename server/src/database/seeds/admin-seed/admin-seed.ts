import { createConnection, Repository } from 'typeorm';
import { UserRole } from '../../../common/enum/roles';
import { User } from '../../entities/user.entity';
import * as bcrypt from 'bcrypt';

// SENSITIVE DATA ALERT! - Normally the seed and the admin credentials should not be present in the public repository!
// Run: `npm run seed` to seed the database

const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);

  const admin = await userRepo.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const name = 'Admin';
  const email = 'test@email.com';
  const username = 'admin';
  const password = 'A555';
  const hashedPassword = await bcrypt.hash(password, 10);

  const newAdmin: User = userRepo.create({
    username,
    password: hashedPassword,
    role: UserRole.ADMIN,
    name,
    email
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
