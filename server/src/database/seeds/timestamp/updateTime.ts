import { Timestamp } from './../../entities/timestamp.entity';
import { Connection, Repository } from 'typeorm';
import { Feeds } from '../../../common/enum/feeds';

export class UpdateTime {
    private readonly timeRepo: Repository<Timestamp>;

    public constructor(private readonly connection: Connection) {
        this.timeRepo = connection.manager.getRepository(Timestamp);
    }

    public async timeStamp(id: Feeds, time: string) {
        const foundStamp = await this.timeRepo.findOne(id);
        if(!foundStamp) {
            const createdStamp = this.timeRepo.create({id, updatedOn: time });
            await this.timeRepo.save(createdStamp);
            return;
        }

        foundStamp.updatedOn = time;
        await this.timeRepo.save(foundStamp);
        return;
    }
}