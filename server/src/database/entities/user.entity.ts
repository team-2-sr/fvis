import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { UserRole } from '../../models/roles';

@Entity()
@Unique(['username'])
export class User {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ nullable: false, length: 35 })
  public name: string;

  @Column({ nullable: false, length: 16 })
  public username: string;

  @Column({ nullable: false, })
  password: string;

  @Column({ nullable: false, length: 60 })
  public email: string;

  @Column('enum', { enum: UserRole, default: UserRole.STANDART })
  public role: UserRole;
}
