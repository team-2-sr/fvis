import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, Index } from 'typeorm';
import { Cpe } from './cpe.entity';
import { Inventory } from './inventory.entity';

@Entity()
export class Cve {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    @Index()
    public cveID: string;

    @Column({ type: 'text' })
    public description: string;

    @Column({nullable: true})
    public severity: string;

    @Column()
    public lastModifiedDate: string;

    @ManyToMany(type => Inventory, inventory => inventory.assignedCves)
    public products: Inventory[];
}