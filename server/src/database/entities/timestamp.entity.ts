import { Entity, Column, PrimaryColumn } from 'typeorm';
import { Feeds } from '../../common/enum/feeds';

@Entity('timestamp')
export class Timestamp {
    @PrimaryColumn()
    public id: Feeds;

    @Column()
    public updatedOn: string;
}