import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, OneToMany, CreateDateColumn, UpdateDateColumn, JoinTable } from 'typeorm';
import { Cpe } from './cpe.entity';
import { Cve } from './cve.entity';

@Entity('inventory')
export class Inventory {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column({nullable: false})
    public product: string;

    @Column({nullable: false})
    public vendor: string;

    @Column({nullable: false})
    public version: string;

    @ManyToOne(type => Cpe, cpe => cpe.products, { eager: true })
    public cpe: Cpe;

    @ManyToMany(type => Cve, cve => cve.products, { eager: true })
    @JoinTable()
    public assignedCves: Cve[];
} 