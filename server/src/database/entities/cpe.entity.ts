import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, OneToMany, CreateDateColumn, UpdateDateColumn, JoinTable } from 'typeorm';
import { Cve } from './cve.entity';
import { Inventory } from './inventory.entity';
@Entity('cpe')
export class Cpe {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column({ nullable: false })
    public cpe23Uri: string;

    @Column()
    public title: string;

    @Column()
    public vendor: string;

    @Column()
    public product: string;

    @Column()
    public version: string;

    @Column()
    public update: string;

    // @Column()
    // public edition: string;

    @OneToMany(type => Inventory, product => product.cpe)
    public products: Inventory[];

    @Column({ default: false })
    public deprecated: boolean;

    // @Column({ default: '' })
    // public depricatedBy: string;

    // @Column({ default: '' })
    // public depricatedOn: string;

    @UpdateDateColumn()
    public updatedOn: Date;

}
