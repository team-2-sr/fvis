import { Controller, UseGuards, Post, HttpCode, HttpStatus, Body, ValidationPipe, Get, Param, ParseIntPipe, Put, Query, Delete, Render } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ShowCpeDto } from '../common/models/show-cpe-dto';
import { CpeAssignService } from './services/cpe-assign.service';
import { CveAssignService } from './services/cve-assign.service';
import { ShowCveDto } from '../common/models/show-cve-dto';

@Controller('api/product')
// @UseGuards(AuthGuard('jwt'))
export class AssignmentController {
    constructor ( private readonly cpeAssignService: CpeAssignService,
                  private readonly cveAssignService: CveAssignService,) {}

    @Get('cpes')
    @HttpCode(HttpStatus.OK)
    public async getCpeMatches(
        @Query('product') product: string,
        @Query('vendor') vendor: string,
        @Query('version') version: string   
        ): Promise<ShowCpeDto[]> {

           return await this.cpeAssignService.getCpeMatches(product, vendor, version);
    }

    @Put(':productId/cpe/:cpeId')
    @HttpCode(HttpStatus.CREATED)
    public async assignProductCpe(
        @Param('productId', new ParseIntPipe()) productId: number, 
        @Param('cpeId', new ParseIntPipe()) cpeId: number
        ): Promise<ShowCpeDto> {

           return await this.cpeAssignService.assignProductCpe(productId, cpeId);
    }

    @Delete(':productId/cpe')
    @HttpCode(HttpStatus.CREATED)
    public async removeProductCpe(
        @Param('productId', new ParseIntPipe()) productId: number
        ): Promise<{message: string}> {

           return await this.cpeAssignService.removeProductCpe(productId);
    }

    @Get('cpe/:cpeId/cve')
    @HttpCode(HttpStatus.OK)
    public async getCveMatches(
        @Param('cpeId', new ParseIntPipe()) cpeId: number
        ): Promise<ShowCveDto[]> {

           return await this.cveAssignService.getCveMatches(cpeId);
    }

    @Put(':productId/cve/:cveId')
    @HttpCode(HttpStatus.CREATED)
    public async assignProductCve(
        @Param('productId', new ParseIntPipe()) productId: number, 
        @Param('cveId', new ParseIntPipe()) cveId: number
        ): Promise<ShowCveDto[]> {

           return await this.cveAssignService.assignProductCve(productId, cveId);
    }

    @Delete(':productId/cve/:cveId')
    @HttpCode(HttpStatus.CREATED)
    public async removeProductCve(
        @Param('productId', new ParseIntPipe()) productId: number,
        @Param('cveId', new ParseIntPipe()) cveId: number
        ): Promise<{message: string}> {

           return await this.cveAssignService.removeProductCve(productId, cveId);
    }
}
