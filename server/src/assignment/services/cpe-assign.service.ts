import { Injectable } from '@nestjs/common';
import { Repository, Like, In, createConnection } from 'typeorm';
import { Cpe } from '../../database/entities/cpe.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Inventory } from '../../database/entities/inventory.entity';
import { TrackingSystemError } from '../../common/exceptions/tracking-system.error';
import { ShowCpeDto } from '../../common/models/show-cpe-dto';
import { ShowInventoryItemDto } from '../../common/models/show-inv-item-dto';

@Injectable()
export class CpeAssignService {
    public constructor(
        @InjectRepository(Cpe) private readonly cpeRepo: Repository<Cpe>,
        @InjectRepository(Inventory) private readonly inventoryRepo: Repository<Inventory>
        ) {}

    public async getCpeMatches(product: string, vendor: string, version: string): Promise<ShowCpeDto[]> {
        if (!product || !vendor || !version) {
            throw new TrackingSystemError('Incorrect query parameters', 400);  
        }

        const vendorHyphen = this.modifyHyphon(vendor);
        const vendorUnderscore = this.modifyUnderscore(vendor);
        const vendorAbriviation = this.modifyAbriviate(vendor);
        const vendorMerged = this.modifyMerge(vendor);

        const productHyphen = this.modifyHyphon(product);
        const productUnderscore = this.modifyUnderscore(product);
        const productMerged = this.modifyMerge(product);

        const versionHyphen = this.modifyHyphon(version);
        const versionUnderscore = this.modifyUnderscore(version);
        const versionMerged = this.modifyMerge(version);

        let cpeMatches;       

        if (version === '*') {
            cpeMatches = await this.cpeRepo.find({
                where: [{
                    title: Like([`%${product}%`]), 
                    vendor:  In([vendorHyphen, vendorUnderscore, vendorMerged, vendorAbriviation]),
                },
                {
                    product: In([productHyphen, productUnderscore, productMerged]), 
                    vendor:  In([vendorHyphen, vendorUnderscore, vendorMerged, vendorAbriviation]),
                }]
            })
        } else {
            cpeMatches = await this.cpeRepo.find({
                where: [{
                    title: Like([`%${product}%`]), 
                    vendor:  In([vendorHyphen, vendorUnderscore, vendorMerged, vendorAbriviation]),
                    version: In([versionHyphen, versionUnderscore, versionMerged, '-'])
                },
                {
                    product: In([productHyphen, productUnderscore, productMerged]), 
                    vendor:  In([vendorHyphen, vendorUnderscore, vendorMerged, vendorAbriviation]),
                    version: In([versionHyphen, versionUnderscore, versionMerged, '-'])
                }]
            })
        }

        cpeMatches.sort((cpeA: Cpe, cpeB: Cpe) => {
            const cpeaVersion = (cpeA.version === versionHyphen || versionUnderscore || versionMerged);
            const cpebVersion = (cpeB.version === versionHyphen || versionUnderscore || versionMerged);
            const cpeaProduct = (cpeA.product === productHyphen || productUnderscore || productMerged);
            const cpebProduct = (cpeB.product === productHyphen || productUnderscore || productMerged);

            if((cpeaVersion && cpeaProduct) && (cpeB.version === '-' || !cpebProduct)) {
                return -1;
            } else if((cpebVersion && cpebProduct) && (cpeA.version === '-' || !cpeaProduct)) {
                return 1;
            }
            return 0;
        });

        return cpeMatches;
    }

    public async assignProductCpe(productId: number, cpeId: number): Promise<ShowCpeDto> {
        const foundInventoryItem = await this.inventoryRepo.findOne(productId);
        const foundCpeItem = await this.cpeRepo.findOne(cpeId);

        if(!foundInventoryItem || !foundCpeItem) {
            throw new TrackingSystemError('Item or cpe does not exist', 400);  
        }

        foundInventoryItem.cpe = foundCpeItem;
        const updatedInvItem = await this.inventoryRepo.save(foundInventoryItem);

        return plainToClass(ShowCpeDto, updatedInvItem.cpe, {excludeExtraneousValues: true});
    }

    public async removeProductCpe(productId: number): Promise<{message: string}> {
        const foundInventoryItem = await this.inventoryRepo.findOne(productId);

        if(!foundInventoryItem) {
            throw new TrackingSystemError('Item does not exist', 400);  
        }

        foundInventoryItem.cpe = null;
        await this.inventoryRepo.save(foundInventoryItem);

        return {message: 'CPE deleted'}
    }

    private modifyHyphon(entry: string): string {
        return entry.toLowerCase().trim().replace(/\s/g, '-').replace(/[^A-Za-z0-9\._\-~%]/g, (match) => `\\${match}`);
    }

    private modifyUnderscore(entry: string): string {
        return entry.toLowerCase().trim().replace(/\s/g, '_').replace(/[^A-Za-z0-9\._\-~%]/g, (match) => `\\${match}`);
    }

    private modifyMerge(entry: string): string {
        return entry.toLowerCase().trim().replace(/\s/g, '').replace(/[^A-Za-z0-9\._\-~%]/g, (match) => `\\${match}`);
    }

    private modifyAbriviate(entry: string): string {
        return entry.toLowerCase().trim().split(' ').map(x => x[0]).join('').replace(/[^A-Za-z0-9\._\-~%]/g, (match) => `\\${match}`);
    }
    
}

// const seed = async () => {
//     console.log('started!');
//     const connection = await createConnection();
//     const cpeRepo = connection.getRepository(Cpe);
//     const inventoryRepo = connection.getRepository(Inventory);
//     const inventoryItem = {id:'1', product: 'Project and Portfolio Management Center', vendor: 'HPE', version: '9.30'}
    
//     const cpeAssignService = new CpeAssignService(cpeRepo, inventoryRepo);
    
//     const res = await cpeAssignService.getCpeMatches(inventoryItem.product, inventoryItem.vendor, inventoryItem.version);
//     console.log(res);
    
//    //const resCpe = await cpeAssignService.assignProductCpe(1, 1);
//    // const resCpe = await cpeAssignService.removeProductCpe(1);
//    // console.log(resCpe);
// //    const cpe = await cpeRepo.findOne({
// //        where: {id: 2},
// //        relations: ['products']
// //    })
// //    console.log(cpe);
   

//     await connection.close();
//     console.log('completed!');
//   };
  
//   seed().catch(console.error);