import { Test, TestingModule } from '@nestjs/testing';
import { CpeAssignService } from './cpe-assign.service';

describe('CpeAssignService', () => {
  let service: CpeAssignService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CpeAssignService],
    }).compile();

    service = module.get<CpeAssignService>(CpeAssignService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
