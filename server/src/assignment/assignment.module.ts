import { Module } from '@nestjs/common';
import { CpeAssignService } from './services/cpe-assign.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cpe } from '../database/entities/cpe.entity';
import { AssignmentController } from './assignment.controller';
import { CveAssignService } from './services/cve-assign.service';
import { Cve } from '../database/entities/cve.entity';
import { CveCpe } from '../database/entities/cve-cpe.entity';
import { Inventory } from '../database/entities/inventory.entity';

@Module({
  imports: [ TypeOrmModule.forFeature([Cpe, Cve, CveCpe, Inventory])],
  providers: [CpeAssignService, CveAssignService],
  controllers: [AssignmentController]
})
export class AssignmentModule {

}
