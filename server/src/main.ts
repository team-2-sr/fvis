import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { TrackingSystemErrorFilter } from './common/filters/tracking-error.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new TrackingSystemErrorFilter());
  app.enableCors();
  await app.listen(app.get(ConfigService).get('PORT'));
}
bootstrap();
