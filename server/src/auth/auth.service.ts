import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { JWTPayload } from './jwt-payload';
import { User } from '../database/entities/user.entity';
import { UserCredentialDto } from '../models/user-credential-dto';
import { ShowUserDto } from '../models/show-user-dto';
import { plainToClass } from 'class-transformer';
import { LogInDto } from '../models/log-in-dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async findUserByName(username: string) {
    return await this.userRepository.findOne({
      where: { username },
    });
  }

  async validateUser(username: string, password: string) {
    const user = await this.findUserByName(username);

    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);

    return isUserValidated ? user : null;
  }

  async login(loginUser: LogInDto): Promise<{ token: string }> {
    const user = await this.validateUser(
      loginUser.username,
      loginUser.password,
    );

    if (!user) {
      throw new UnauthorizedException('Wrong credentials!');
    }

    const payload: JWTPayload = {
      id: user.id,
      name: user.username,
      role: user.role,
    };

    const token = await this.jwtService.signAsync(payload);

    return {
      token,
    };
  }

  async createUser(
    userCredentialsDto: UserCredentialDto,
  ): Promise<ShowUserDto> {
    const userToView: User = await this.userRepository.findOne(
      userCredentialsDto.username,
    );
    if (userToView) {
      throw new BadRequestException('User with this username already exists!');
    }
    const createdUser = await this.userRepository.create(userCredentialsDto);
    let savedUser;

    try {
      createdUser.password = await bcrypt.hash(userCredentialsDto.password, 10);
      savedUser = await this.userRepository.save(createdUser);
    } catch (error) {
      throw new BadRequestException('Not valid credentials!');
    }

    return plainToClass(ShowUserDto, savedUser, {
      excludeExtraneousValues: true,
    });
  }
}
