import {
  Controller,
  Post,
  Body,
  Delete,
  UseGuards,
  HttpCode,
  HttpStatus,
  ValidationPipe,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserCredentialDto } from '../models/user-credential-dto';
import { AuthGuard } from '@nestjs/passport';
import { ShowUserDto } from '../models/show-user-dto';
import { LogInDto } from '../models/log-in-dto';
import { AdminGuard } from '../common/guards/admin.guard';

@Controller('session')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  public async login(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    user: LogInDto,
  ): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete()
  @UseGuards(AuthGuard('jwt'))
  public async logoutUser() {
    return {
      msg: 'Successful logout!',
    };
  }

  @Post('/users')
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  @HttpCode(HttpStatus.CREATED)
  public async createNewUser(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    userCredentialsDto: UserCredentialDto,
  ): Promise<ShowUserDto> {
    return await this.authService.createUser(userCredentialsDto);
  }
}
