import { UserRole } from "../models/roles";

export class JWTPayload {
  id: number;
  name: string;
  role: UserRole
}
