import { Controller, Put, HttpCode, ParseIntPipe, Param, HttpStatus } from '@nestjs/common';
import { EmailService } from './services/mail.service';

@Controller('api/email')
export class EmailController {

    constructor (private readonly emailService: EmailService) {}

    @Put(':productId')
    @HttpCode(HttpStatus.CREATED)
    public async sentEmailAlert(
        @Param('productId', new ParseIntPipe()) productId: number
        ): Promise<{ message: string }> {

         await this.emailService.sentEmailAlert(productId);

        return { message: 'Email alert successfully sent!' };
    }
}
