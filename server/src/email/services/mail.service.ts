import { plainToClass } from 'class-transformer';
import { Injectable } from '@nestjs/common';
import { Inventory } from '../../database/entities/inventory.entity';
import { Repository, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { MailerService } from '@nestjs-modules/mailer';
import { ShowCpeDto } from '../../common/models/show-cpe-dto';
import { ShowCveDto } from '../../common/models/show-cve-dto';
import { ShowInventoryItemDto } from '../../common/models/show-inv-item-dto';

@Injectable()
export class EmailService {
    public constructor(
        @InjectRepository(Inventory) private readonly inventoryRepo: Repository<Inventory>,
        private readonly mailerService: MailerService
        ) {}
        
    public async sentEmailAlert(productId) {

        let products: Inventory[] = await this.inventoryRepo.find({
            where:  { id: Like(productId) }
          });

          const foundProducts = (products as any).map(product => {
            product.cpe = plainToClass(ShowCpeDto, product.cpe, {
              excludeExtraneousValues: true,
            });
            product.assignedCves = (product.assignedCves as any).map(cve =>
              plainToClass(ShowCveDto, cve, { excludeExtraneousValues: true }),
            );
      
            return plainToClass(ShowInventoryItemDto, product, {
              excludeExtraneousValues: true,
            });
          });

          const cpes = JSON.stringify(foundProducts[0].cpe)

        await this
          .mailerService
          .sendMail({
            to: 'jurists@abv.bg', // list of receivers
            from: '"No Reply" <no-reply@localhost>', // sender address
            subject: 'Notification for vulnarable software', // Subject line
            template: 'assignment', // plaintext body
            context:{
            product: foundProducts[0].product,
            vendor: foundProducts[0].vendor,
            version: foundProducts[0].version,
            cpes : JSON.stringify(foundProducts[0].cpe),
            cves : JSON.stringify(foundProducts[0].assignedCves)
        }
            // html: 
            // 'product: '+JSON.stringify(foundProducts[0].product)+'<br>'+
            // 'vendor: '+JSON.stringify(foundProducts[0].vendor)+'<br>'+ 
            // 'version: '+JSON.stringify(foundProducts[0].version)+'<br>'+
            // 'CPE : '+JSON.stringify(foundProducts[0].cpe)+'<br>'+
            // 'CVEs : '+JSON.stringify(foundProducts[0].cve)+'<br>'
          })
          .then(() => {})
          .catch((e) => {console.log(e);
          });

          return foundProducts;
      }
}