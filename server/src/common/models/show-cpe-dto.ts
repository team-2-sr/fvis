import { Expose, Transform } from 'class-transformer';

export class ShowCpeDto {
    @Expose()
    public id: number;

    @Expose()
    public cpe23Uri: string;

    @Expose()
    public title: string;

    @Expose()
    public vendor: string;

    @Expose()
    public product: string;

    @Expose()
    public version: string;

    @Expose()
    public update: string;

    @Expose()
    public deprecated: boolean;

    // @Expose()
    // public depricatedBy: string | null;

    // @Expose()
    // public depricatedOn: string | null;
}