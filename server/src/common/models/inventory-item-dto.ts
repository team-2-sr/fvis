import { IsString, IsNotEmpty } from "class-validator";
import { ShowCpeDto } from "./show-cpe-dto";

export class InventoryItemDto {
    @IsString()
    @IsNotEmpty()
    product: string;

    @IsString()
    @IsNotEmpty()
    vendor: string;

    @IsString()
    @IsNotEmpty()
    version: string;
}