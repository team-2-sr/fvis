import { Expose, Transform } from 'class-transformer';
import { ShowCpeDto } from "./show-cpe-dto";
import { ShowCveDto } from './show-cve-dto';

export class ShowInventoryItemDto {
    @Expose()
    id: number;

    @Expose()
    product: string;

    @Expose()
    vendor: string;

    @Expose()
    version: string;
    
    @Expose()
    cpe: ShowCpeDto | null;

    @Expose()
    assignedCves: ShowCveDto[];
}