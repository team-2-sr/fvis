export enum UpdateName {
    CPE_UPDATE = 'cpe-update',
    CVE_UPDATE = 'cve-update'
}