export enum Feeds {
    CVE = 'CVE',
    CPE = 'CPE',
    CPE_SEEDING = 'CPE_SEEDING',
    CVE_SEEDING = 'CVE_SEEDING'
}