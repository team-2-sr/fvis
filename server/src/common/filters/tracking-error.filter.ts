import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { TrackingSystemError } from '../exceptions/tracking-system.error';


@Catch(TrackingSystemError)
export class TrackingSystemErrorFilter implements ExceptionFilter {
  public catch(exception: TrackingSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
