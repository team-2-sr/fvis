import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Inventory } from '../database/entities/inventory.entity';
import { ProductsController } from './products.controller';
import { ProductsService } from './services/products.service';

@Module({
    imports: [ TypeOrmModule.forFeature([Inventory])],
    controllers: [ProductsController],
    providers: [ProductsService],
})
export class ProductsModule {}
