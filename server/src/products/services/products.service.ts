import { plainToClass } from 'class-transformer';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Inventory } from '../../database/entities/inventory.entity';
import { Repository } from 'typeorm';
import { ShowInventoryItemDto } from '../../common/models/show-inv-item-dto';
import { ShowCpeDto } from '../../common/models/show-cpe-dto';
import { ShowCveDto } from '../../common/models/show-cve-dto';
import { InventoryItemDto } from '../../common/models/inventory-item-dto';
import { TrackingSystemError } from '../../common/exceptions/tracking-system.error';

@Injectable()
export class ProductsService {
    public constructor(
        @InjectRepository(Inventory) private readonly inventoryRepo: Repository<Inventory>
        ) {}

    public async getAllProducts(): Promise<ShowInventoryItemDto[]> {
        const foundProducts = await this.inventoryRepo.find({
                relations: ['cpe', 'assignedCves']
            })

        const foundProductsToDto = (foundProducts as any).map(product => {
                product.cpe = plainToClass(ShowCpeDto, product.cpe, {excludeExtraneousValues: true})
                product.assignedCves = (product.assignedCves as  any).map(cve => plainToClass(ShowCveDto, cve, {excludeExtraneousValues: true}))
                
                return plainToClass(ShowInventoryItemDto, product, {excludeExtraneousValues: true});
            });

        return foundProductsToDto;
    }

    public async getProductsByAssignCves(assignedCves: boolean): Promise<ShowInventoryItemDto[]> {
        let foundProducts = await this.inventoryRepo.find({
            relations: ['cpe', 'assignedCves']
        })

        foundProducts = assignedCves
            ? foundProducts.filter(product => product.assignedCves.length > 0)
            : foundProducts.filter(product => product.assignedCves.length === 0);

        const foundProductsToDto = (foundProducts as any).map(product => {
            product.cpe = plainToClass(ShowCpeDto, product.cpe, {excludeExtraneousValues: true})
            product.assignedCves = (product.assignedCves as  any).map(cve => plainToClass(ShowCveDto, cve, {excludeExtraneousValues: true}))
            
            return plainToClass(ShowInventoryItemDto, product, {excludeExtraneousValues: true});
        });

        return foundProductsToDto;
    }

    public async getOneProduct(productId: number): Promise<ShowInventoryItemDto> {        
        const foundProduct = await this.inventoryRepo.findOne({
            where: {id: productId},
            relations: ['cpe', 'assignedCves']
        })

        if(!foundProduct) {
            throw new TrackingSystemError('This product does not exist', 400);  
        }

        const plainFoundProduct = foundProduct as any;
        plainFoundProduct.cpe = plainToClass(ShowCpeDto, foundProduct.cpe, {excludeExtraneousValues: true});
        plainFoundProduct.assignedCves = foundProduct.assignedCves.map(cve => plainToClass(ShowCveDto, cve, {excludeExtraneousValues: true}))

        return plainToClass(ShowInventoryItemDto, plainFoundProduct, {excludeExtraneousValues: true});
    }
    
    public async addNewProduct(item: InventoryItemDto): Promise<ShowInventoryItemDto> {
        const foundItem = await this.inventoryRepo.findOne({
            where: {product: item.product, vendor: item.vendor, version: item.version}
        })

        if(foundItem) {
            throw new TrackingSystemError('This product already exist', 403);  
        }

        const createdNewItem = this.inventoryRepo.create({
            product: item.product,
            vendor: item.vendor,
            version: item.version
        });
        createdNewItem.assignedCves = [];

        const savedNewItem = await this.inventoryRepo.save(createdNewItem);

        return plainToClass(ShowInventoryItemDto, savedNewItem, {excludeExtraneousValues: true})
    }
}
