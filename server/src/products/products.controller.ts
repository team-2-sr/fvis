import { Controller, Get, HttpCode, HttpStatus, Query, Post, ValidationPipe, Body, ParseIntPipe, Param } from '@nestjs/common';
import { ProductsService } from './services/products.service';
import { ShowInventoryItemDto } from '../common/models/show-inv-item-dto';
import { InventoryItemDto } from '../common/models/inventory-item-dto';

@Controller('api/product')
export class ProductsController {
    constructor ( private readonly productsService: ProductsService
       ) {}
    
    @Get()
    @HttpCode(HttpStatus.OK)
    public async getAllProducts(): Promise<ShowInventoryItemDto[]> {

            return await this.productsService.getAllProducts();
    }

    @Get('cves')
    @HttpCode(HttpStatus.OK)
    public async getProductsByAssignCves(
        @Query('assigned') assignedCves: boolean
        ): Promise<ShowInventoryItemDto[]> {

            return await this.productsService.getProductsByAssignCves(assignedCves);
    }

    @Get(':id')
    @HttpCode(HttpStatus.OK)
    public async getOneProduct(
        @Param('id', new ParseIntPipe()) productId: number
        ): Promise<ShowInventoryItemDto> {

            return await this.productsService.getOneProduct(productId);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    public async addNewProduct(
        @Body(new ValidationPipe({ transform: true, whitelist: true })) item: InventoryItemDto, 
        ): Promise<ShowInventoryItemDto> {

           return await this.productsService.addNewProduct(item);
    }
}
