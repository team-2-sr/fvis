import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { AssignmentModule } from './assignment/assignment.module';
import { ProductsModule } from './products/products.module';
import { SearchModule } from './search/search.module';
import { UpdateModule } from './update/update.module';
import { EmailModule } from './email/email.module';

@Module({
  imports: [
    CoreModule,
    DatabaseModule,
    AuthModule,
    AssignmentModule,
    ProductsModule,
    SearchModule,
    UpdateModule,
    EmailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}